<?php
/**
 * i-doit PHP API Client
 *
 * Copyright (c) 2014 Dennis Stücken
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package   $Package$
 * @version   $Version$
 * @copyright Dennis Stücken
 * @author    Dennis Stücken <dstuecken@i-doit.de>
 * @license   http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 */

/**
 * Namespace aliases
 */
use idoit\Api\Client as ApiClient;
use idoit\Api\Connection as ApiConnection;

try
{
    /**
     * i-doit
     *
     * Api usage example
     *
     * @package    i-doit
     * @subpackage api
     * @author     DS, 2014
     */
    include_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'apiclient.php');
    include_once(__DIR__ . DIRECTORY_SEPARATOR . 'config.php');

    \idoit\Api\Config::$jsonRpcDebug = false;

    /* --------------------------------------------------------- */
    /* Initalize */
    /* --------------------------------------------------------- */
    $apiClient = new ApiClient(
        new ApiConnection(
            $api_entry_point, $api_key
        )
    );

    echo '<pre>';

    // object ID => datasource ID (which may also represent the name of an object).
    $datasources = [
        123  => 'DEU001823',
        126  => 'DEU06426',
        99   => 'DEU06426',
        100  => 'DEU06426',
        134  => 'USA0012343',
        64   => 'CHR00182642',
        7457 => 'FRA00164262',
    ];

    $objAPI = new idoit\Api\CMDB\Object($apiClient);
    $categoryAPI = new idoit\Api\CMDB\Category($apiClient);

    foreach ($datasources as $objID => $datasource) {

        echo PHP_EOL . $objID . ' mit Datasource "' . $datasource . '"...' . PHP_EOL;

        // Checking the datasource for some specific conditions.
        if (strlen($datasource) !== 8 || substr($datasource, 0, 3) !== 'DEU' || !is_numeric(substr($datasource, 3)))
        {
            echo 'Die Datasource "' . $datasource . '" ist nicht korrekt!' . PHP_EOL;
            continue;
        } // if

        // For more information...
        //$obj = $objAPI->search($datasource, \idoit\Api\CMDB\ObjectTypeConstants::TYPE_CITY);
        $locationParentObjID = $objAPI->getIdByTitle($datasource, \idoit\Api\CMDB\ObjectTypeConstants::TYPE_CITY);

        if ($locationParentObjID > 0)
        {
            // Assign the location parent, if it has none.
            $location = $categoryAPI->get($objID, \idoit\Api\CMDB\CategoryConstants::G_LOCATION);

            if (is_array($location) && isset($location[0]['parent']['id']) && $location[0]['parent']['id'] > 0)
            {
                echo 'Der vorhandene Standort soll nicht überschrieben werden!' . PHP_EOL;

                // Dont update a given location.
                continue;
            } // if

            echo 'Standort zuweisen!' . PHP_EOL;

            // Assign the new location.
            $objAPI->updateLocation($objID, $locationParentObjID);
        } // if
    } // foreach

    echo '</pre>';
}
catch (Exception $e)
{
    // Handle error message.
    echo '<pre>';
    print_r($e->getMessage());
    echo '</pre>';
} // try