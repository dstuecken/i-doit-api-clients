<?php
/**
 * i-doit PHP API Client
 * Copyright (c) 2014 Leonard Fischer
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package   $Package$
 * @version   $Version$
 * @copyright Leonard Fischer
 * @author    Leonard Fischer <lfischer@i-doit.com>
 * @license   http://opensource.org/licenses/MIT The MIT License (MIT)
 */

/**
 * Namespace aliases
 */
use idoit\Api\Client as ApiClient;
use idoit\Api\Connection as ApiConnection;

try
{
    include_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'apiclient.php');
    include_once(__DIR__ . DIRECTORY_SEPARATOR . 'config.php');

    /* --------------------------------------------------------- */
    /* Initalize                                                 */
    /* --------------------------------------------------------- */
    $l_apiClient = new ApiClient(new ApiConnection($api_entry_point, $api_key));
    $l_object_id = 24;
    $l_object_ids = [24, 25, 26];

    $l_categoryApi = new \idoit\Api\Request($l_apiClient, 'cmdb.workflow.create', [
        'title' => 'Workflow, created by API',
        'object_id' => $l_object_id,
        // Alternative: 'object_ids' => $l_object_ids
        'type' => '1', // 1: task, 2: checklist

        // Optional...
        'category' => 'My category name',
        'creator' => 'editor', // The "creator" of this workflow (object title).
        'start_date' => '2016-01-01',
        'end_date' => '2016-12-31',
        'contact_id' => [9], // By default the object with ID #9 is the administrator.
        'description' => 'Some custom description',
    ]);

    print_r($l_categoryApi->send());
}
catch (Exception $e)
{
    print_r($e->getMessage());
    echo "\n";
}