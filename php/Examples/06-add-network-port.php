<?php
/**
 * i-doit PHP API Client
 *
 * Copyright (c) 2016 Dennis Stücken
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package   $Package$
 * @version   $Version$
 * @copyright Dennis Stücken
 * @author    Dennis Stücken <dstuecken@i-doit.com>
 * @license   http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 */

/**
 * Namespace aliases
 */
use idoit\Api\Client as ApiClient;
use idoit\Api\CMDB\Object as CMDBObject;
use idoit\Api\CMDB\Category as CMDBCategory;
use idoit\Api\CMDB\CategoryConstants as Category;
use idoit\Api\CMDB\ObjectTypeConstants as ObjectType;
use idoit\Api\Connection as ApiConnection;

try
{
    /**
     * i-doit
     *
     * Api usage example
     *
     * @package    i-doit
     * @subpackage api
     * @author     DS, 2014
     */
    include_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'apiclient.php');
    include_once(__DIR__ . DIRECTORY_SEPARATOR . 'config.php');


    /* --------------------------------------------------------- */
    /* Initalize */
    /* --------------------------------------------------------- */
    $l_apiClient = new ApiClient(
        new ApiConnection(
            $api_entry_point, $api_key
        )
    );

    /* --------------------------------------------------------- */
    /* Get object named by Fileserver02 */
    /* --------------------------------------------------------- */
    $l_objectApi = new CMDBObject($l_apiClient);
    $l_objectID  = $l_objectApi->getIdByTitle('Fileserver02', ObjectType::TYPE_SERVER);

    /**
     * If object exists, create 10 Ports
     */
    if ($l_objectID > 0)
    {
        /* Add Loopback IP Address */
        $l_category     = new \idoit\Api\CMDB\Category($l_apiClient);
        $l_categoryData = new \idoit\Api\CMDB\Category\G\Ip();
        $l_categoryData->setIpv4Address('127.0.0.1');
        $l_categoryData->setHostname('localhost');
        $l_category->add($l_objectID, $l_categoryData);

		for ($i = 1; $i <= 1; $i++)
		{
			$title = 'Port ' . $i;

            /* Add network port */
			$l_categoryApi = new CMDBCategory($l_apiClient);
			$l_addPort     = $l_categoryApi->add(
				$l_objectID,
				Category::G_SUBCAT_NETWORK_PORT,

				array(
					'title'             => $title,
					'port_type'         => 'Ethernet',
					'port_mode'         => '1',
					'plug_type'         => 'RJ-45',
					'negotiation'       => '1',
					'duplex'            => '2',
					'speed'             => '1000',
					'speed_type'        => '3',
					'standard'          => '1',
					'mac'               => '00:00:00:00:00:00',
					'active'            => '1',
					'addresses'         => array('172.16.0.3', '127.0.0.1'), // also attaching loopback address to port to
                                                                             // show possibility of attaching more than one address
					'layer2_assignment' => array(8600, 8727), // These integers are the layer 2 net's object addresses
					'description'       => ''
				)
			);
			if (isset($l_addPort['success']) && $l_addPort['success'])
			{

				echo $title . " added to object {$l_objectID}.\n";

			} else {
			    throw new Exception('Error while adding Port ' . $i);
			}
		}
	} else {
	    throw new Exception('Object not found');
	}

}
catch (Exception $e)
{
	print_r($e->getMessage());
	echo "\n";
}