<?php
/**
 * i-doit PHP API Client
 * Copyright (c) 2016 Leonard Fischer
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package   $Package$
 * @version   $Version$
 * @copyright Leonard Fischer
 * @author    Leonard Fischer <lfischer@i-doit.com>
 * @license   http://opensource.org/licenses/MIT The MIT License (MIT)
 */

/**
 * Namespace aliases
 */
use idoit\Api\Client as ApiClient;
use idoit\Api\CMDB\Object as CMDBObject;
use idoit\Api\CMDB\Category as CMDBCategory;
use idoit\Api\Connection as ApiConnection;

/**
 * @param  idoit\Api\Client  $apiClient
 * @param  integer           $objServerID
 * @param  integer           $objPersonID
 */
function assignPerson($apiClient, $objServerID, $objPersonID)
{
    $categoryAPI = new CMDBCategory($apiClient);
    $contactCategoryData = new idoit\Api\CMDB\Category\G\Contact();

    $existing = in_array(
        $objPersonID,
        (array) \idoit\Lib\Arr::path(
            $categoryAPI->get($objServerID, $contactCategoryData->getCategoryIdentifier()),
            '*.contact_object.id'));

    if (!$existing)
    {
        $contactCategoryData
            ->setContactObject($objPersonID)
            ->setPrimary(true);

        $response = $categoryAPI->add($objServerID, $contactCategoryData);

        if ($response['success'])
        {
            print_r("Successfully added person with id $objPersonID to server with id $objServerID" . PHP_EOL);
        }
        else
        {
            print_r("Failed to add person with id $objPersonID to server with id $objServerID: " . $response['message'] . PHP_EOL);
        } // if
    }
    else
    {
        print_r("Contact with id $objPersonID already assigned to server with id $objServerID" . PHP_EOL);
    } // if
} // function

/**
 * @param  idoit\Api\Client $apiClient
 * @param  integer          $objServerID
 * @param  integer          $portName
 * @return mixed
 */
function createPort($apiClient, $objServerID, $portName)
{
    $l_categoryApi = new CMDBCategory($apiClient);
    $contactCategoryData = (new idoit\Api\CMDB\Category\G\SubcatNetworkPort())
        ->setData([
            'title'             => $portName,
            'port_type'         => 'Ethernet',
            'port_mode'         => '1',
            'plug_type'         => 'RJ-45',
            'negotiation'       => '1',
            'duplex'            => '2',
            'speed'             => '1000',
            'speed_type'        => '3',
            'standard'          => '1',
            'mac'               => '00:00:00:00:00:00',
            'active'            => '1'
        ]);

    $newPort = $l_categoryApi->add($objServerID, $contactCategoryData);
    $ports = $l_categoryApi->get($objServerID, \idoit\Api\CMDB\CategoryConstants::G_SUBCAT_NETWORK_PORT);

    print_r($newPort);
    print_r($ports);

    if (isset($newPort['success']) && $newPort['success'])
    {
        echo "New port '" . $portName . "' added to object " . $objServerID . PHP_EOL;

        return $newPort['id'];
    }
    else
    {
        echo "The port '" . $portName . "' could not be created in object " . $objServerID . PHP_EOL;

        return null;
    } // if
} // function

/**
 * Connect on port to another one.
 *
 * @param idoit\Api\Client $apiClient
 * @param integer          $objServerID
 * @param integer          $portA
 * @param integer          $portB
 */
function connectPort($apiClient, $objServerID, $portA, $portB)
{
    $l_categoryApi = new CMDBCategory($apiClient);
    $contactCategoryData = (new idoit\Api\CMDB\Category\G\Connector())
        ->setData(['category_id' => $portA])
        ->setAssignedConnector($portB);

    // Although we connect two "ports" we need to use the "connector" category, because this serves as a connector-supervisor.
    $ports = $l_categoryApi->update($objServerID, \idoit\Api\CMDB\CategoryConstants::G_CONNECTOR, $contactCategoryData);

    // Retrieve the category entry IDs from the "Connector", not "Port".
    //$ports = $l_categoryApi->get($objServerID, \idoit\Api\CMDB\CategoryConstants::G_CONNECTOR);
    print_r($ports);
} // function

try
{
    /**
     * i-doit
     * Api usage example
     *
     * @package    i-doit
     * @subpackage api
     * @author     DS, 2014
     */
    include_once(dirname(__DIR__) . '/apiclient.php');
    include_once(__DIR__ . '/config.php');

    /* --------------------------------------------------------- */
    /* Initalize                                                 */
    /* --------------------------------------------------------- */
    \idoit\Api\Config::$jsonRpcDebug = false;

    $apiClient = new ApiClient(new ApiConnection($api_entry_point, $api_key));

    /* --------------------------------------------------------- */
    /* Object creation                                           */
    /* --------------------------------------------------------- */
    $serverTitle = 'My API Server';
    $objectAPI = new CMDBObject($apiClient);
    $categoryAPI = new CMDBCategory($apiClient);

    // Search for our object (so we don't create a dozen of new objects).
    $objServer = $objectAPI->search($serverTitle, \idoit\Api\CMDB\ObjectTypeConstants::TYPE_SERVER);

    echo '<pre>';

    if (is_array($objServer) && count($objServer))
    {
        print_r('Object found!' . PHP_EOL);
        //print_r($objServer);
        $objServerID = $objServer[0]['id'];
    }
    else
    {
        print_r('Object not found - create a new one!' . PHP_EOL);

        // Create the new Server object.
        $objServer = $objectAPI->createObject($serverTitle, \idoit\Api\CMDB\ObjectTypeConstants::TYPE_SERVER);

        if (!$objServer['success'])
        {
            throw new Exception('Object konnte nicht erstellt werden: ' . var_export($l_newObject, true));
        } // if

        $objServerID = $objServer['id'];
    } // if

    print_r('Object ID: ' . $objServerID . PHP_EOL);

    print_r('Update description and SYS-ID' . PHP_EOL);

    // Updating general category.
    $generalCategoryData = (new \idoit\Api\CMDB\Category\G\General)
        ->setSysId('Test')
        ->setDescription('Geupdated am: ' . date('d.m.Y H:i:s'));

    print_r($categoryAPI->update($objServerID, \idoit\Api\CMDB\CategoryConstants::G_GLOBAL, $generalCategoryData));

    // print_r($categoryAPI->get($objServerID, \idoit\Api\CMDB\CategoryConstants::G_GLOBAL));

    // Set location.
    $objectAPI->updateLocation($objServerID, 24);

    // Retrieve a person object.
    $objPersonID = $objectAPI->getIdByTitle('Admin Istrator Lord of Rack', \idoit\Api\CMDB\ObjectTypeConstants::TYPE_PERSON);

    if ($objPersonID > 0)
    {
        // Assign a person to a server.
        assignPerson($apiClient, $objServerID, $objPersonID);
    } // if

    // Create a new port.
    //$newPortID = createPort($apiClient, $objServerID, 'Port Name');

    // Connect two ports (connectors)
    connectPort($apiClient, $objServerID, 21, 22);

    // Delete object.
    // $objectAPI->delete($serverTitle);

    echo '</pre>';
}
catch (Exception $e)
{
    print_r($e->getMessage());
    echo "\n";
} // try

