<?php
/**
 * i-doit PHP API Client
 *
 * Copyright (c) 2016 Dennis Stücken
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package   $Package$
 * @version   $Version$
 * @copyright Dennis Stücken
 * @author    Dennis Stücken <dstuecken@i-doit.com>
 * @license   http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 */

$default_api_host = 'http://demo.i-doit.com/src/jsonrpc.php';

if ($argc > 1 && isset($argv[1])) {
    $api_key = $argv[1];
}
if ($argc > 2 && isset($argv[2])) {
    $api_host = $argv[2];
}

/* ----------------------------------------------------------------------------------------  */
include_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'Autoload.inc.php');
/* ----------------------------------------------------------------------------------------  */

// Bash colors.
define("C__COLOR__WHITE", "\033[1;37m");
define("C__COLOR__BLACK", "\033[0;30m");
define("C__COLOR__BLUE", "\033[0;34m");
define("C__COLOR__GREEN", "\033[0;32m");
define("C__COLOR__CYAN", "\033[0;36m");
define("C__COLOR__RED", "\033[0;31m");
define("C__COLOR__PURPLE", "\033[0;35m");
define("C__COLOR__BROWN", "\033[0;33m");
define("C__COLOR__LIGHT_GRAY", "\033[0;37m");
define("C__COLOR__DARK_GRAY", "\033[1;30m");
define("C__COLOR__LIGHT_BLUE", "\033[1;34m");
define("C__COLOR__LIGHT_GREEN", "\033[1;32m");
define("C__COLOR__LIGHT_CYAN", "\033[1;36m");
define("C__COLOR__LIGHT_RED", "\033[1;31m");
define("C__COLOR__LIGHT_PURPLE", "\033[1;35m");
define("C__COLOR__YELLOW", "\033[1;33m");
define("C__COLOR__NO_COLOR", "\033[0m");

echo C__COLOR__WHITE . "\nInitializing i-doit APi-Client...\n" . C__COLOR__NO_COLOR;

if (!isset($api_host)) {
    echo C__COLOR__LIGHT_RED . "\ni-doit APi entry-point [{$default_api_host}]: " . C__COLOR__NO_COLOR;

    $api_host = trim(fgets(STDIN));
    if (!$api_host) {
        $api_host = $default_api_host;
    }
}

if (!isset($api_key)) {
    echo C__COLOR__LIGHT_RED . "\ni-doit APi-Key of your tenant you would like to bind to: " . C__COLOR__NO_COLOR;

    $api_key = trim(fgets(STDIN));
}

$time_start = microtime(true);
$created_files = 0;

if (!$api_key) {
    echo C__COLOR__RED . "\nError. Api-Key empty.\nExiting..\n\n" . C__COLOR__NO_COLOR;
    exit;
}

echo C__COLOR__BROWN . "\n\nOK.. Updating APi codeset..\n" . C__COLOR__NO_COLOR;

try {
    $jsonRpc = new \idoit\Lib\Jsonrpc($api_host);

    /**
     * Retrieve i-doit Version info
     */
    $versionInfo = $jsonRpc->request('idoit.version', array('apikey' => $api_key));

    if (!$versionInfo) {
        throw new Exception('Error. i-doit api did not answer as expected.');
    }

    $idoitVersion = $versionInfo['version'];

    echo C__COLOR__WHITE . 'Connected to i-doit Version ' . $idoitVersion . ' ' . $versionInfo['type'] . "\n\n" . C__COLOR__NO_COLOR;

    /**
     * Get all i-doit relevant constants
     */
    $constants = $jsonRpc->request('idoit.constants', array('apikey' => $api_key));

    /**
     * Prepare Autogeneration of php files
     */
    $year = date('Y');

    $phpHeader = '<?php
/**
 * i-doit PHP API Client
 *
 * Copyright (c) $year Dennis Stücken
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package   i-doit APi-Client (Autogenerated)
 * @version   $idoitVersion
 * @copyright Dennis Stücken
 * @author    Dennis Stücken <dstuecken@i-doit.com>
 * @license   http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 */
';

    /**
     * Write ObjectTypeConstants.php content
     */
    $exportedConstants = '';
    if (isset($constants['objectTypes']) && is_array($constants['objectTypes'])) {
        foreach ($constants['objectTypes'] as $const => $title) {
            $exportedConstants .= "\n\n\t/**\n\t * " . $title . "\n\t */\n";
            $exportedConstants .= "\tconst TYPE_" . strtoupper(str_replace('C__OBJECT_TYPE__', '', str_replace('C__OBJTYPE__', '', $const))) . " = '$const';";
        }
    }

    $objectTypeFileContent = $phpHeader . '

namespace idoit\Api\CMDB;

/**
 * Class ObjectTypeConstants
 * @package idoit\Api\CMDB
 */
class ObjectTypeConstants
{ $exportedConstants
}
';

    /**
     * Write CategoryConstants.php content
     */
    echo 'Writing dynamic codesets into idoit/Api/CMDB/..';

    $exportedConstants = '';
    if (isset($constants['categories']['g']) && is_array($constants['categories']['g'])) {
        foreach ($constants['categories']['g'] as $const => $title) {
            progress();
            $catData = array();
            $catName = str_replace('Subcat', '', strtoupper(str_replace('C__CMDB__SUBCAT__', 'SUBCAT_', str_replace('C__CATS__', '', str_replace('C__CATG__', '', $const)))));
            $exportedConstants .= "\n\n\t/**\n\t * " . $title . "\n\t * @category Global\n\t */\n";
            $exportedConstants .= "\tconst G_" . $catName . " = '$const';";

            try {
                /**
                 * Get category info of $const
                 */
                $categoryInfo = $jsonRpc->request(
                    'cmdb.category_info', array(
                        'apikey' => $api_key, 'catgID' => $const
                    )
                );

                foreach ($categoryInfo as $key => $value) {
                    $catData[] = array(
                        'key' => $key,
                        'type' => @$value['data']['type'],
                        'db_field' => @$value['data']['field'],
                        'db_table' => @$value['data']['table_alias'],
                        'readonly' => @$value['data']['readonly'],
                        'objectTypeFilter' => @$value['ui']['params']['typeFilter'],
                        'plugin_type' => @$value['info']['type'],
                        'mandatory' => !!@$value['check']['mandatory'],
                        'description' => @$value['info']['description']
                    );
                }
                writeCategory(str_replace('_', ' ', $catName), 'G', $catData, $const);
            } catch (\idoit\Api\Exception $e) {
                ;
            } catch (Exception $e) {
                ;
            }
        }
    }

    if (isset($constants['categories']['s']) && is_array($constants['categories']['s'])) {
        $exportedConstants .= "\n";
        foreach ($constants['categories']['s'] as $const => $title) {
            progress();
            $catData = array();
            $catName = str_replace('Subcat', '', strtoupper(str_replace('C__CMDB__SUBCAT__', 'SUBCAT_', str_replace('C__CATS__', '', str_replace('C__CATG__', '', $const)))));
            $exportedConstants .= "\n\n\t/**\n\t * " . $title . "\n\t * @category Specific\n\t */\n";
            $exportedConstants .= "\tconst S_" . $catName . " = '$const';";

            /**
             * Get category info of $const
             */
            $categoryInfo = $jsonRpc->request('cmdb.category_info', array('apikey' => $api_key, 'catsID' => $const));

            foreach ($categoryInfo as $key => $value) {
                $catData[] = array(
                    'key' => $key,
                    'type' => @$value['data']['type'],
                    'db_field' => @$value['data']['field'],
                    'db_table' => @$value['data']['table_alias'],
                    'readonly' => @$value['data']['readonly'],
                    'objectTypeFilter' => @$value['ui']['params']['typeFilter'],
                    'plugin_type' => @$value['info']['type'],
                    'mandatory' => !!@$value['check']['mandatory'],
                    'description' => @$value['info']['description']
                );
            }
            writeCategory(str_replace('_', ' ', $catName), 'S', $catData, $const);
        }
    }

    $categoryFileContent = $phpHeader . '

namespace idoit\Api\CMDB;

/**
 * Class CategoryConstants
 * @package idoit\Api\CMDB
 */
class CategoryConstants
{ $exportedConstants
}
';

    /**
     * ------------------------------------------------------------------------------------------------------------------------------
     * Dynamically write ObjectTypeConstants.php file
     * ------------------------------------------------------------------------------------------------------------------------------
     */
    $objectTypeConstantsFilePath = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'Api' . DIRECTORY_SEPARATOR . 'CMDB' . DIRECTORY_SEPARATOR . 'ObjectTypeConstants.php';
    if (!file_put_contents($objectTypeConstantsFilePath, $objectTypeFileContent)) {
        throw new Exception('Could not create file ' . $objectTypeConstantsFilePath);
    } else {
        $created_files++;
        //echo "\nDynamic codeset {$objectTypeConstantsFilePath} written..\n";
    }

    /**
     * ------------------------------------------------------------------------------------------------------------------------------
     * Dynamically write CategoryConstants.php file
     * ------------------------------------------------------------------------------------------------------------------------------
     */
    $categoryConstantsFilePath = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'Api' . DIRECTORY_SEPARATOR . 'CMDB' . DIRECTORY_SEPARATOR . 'CategoryConstants.php';
    if (!file_put_contents($categoryConstantsFilePath, $categoryFileContent)) {
        throw new Exception('Could not create file ' . $categoryConstantsFilePath);
    } else {
        $created_files++;
        //echo "Dynamic codeset {$categoryConstantsFilePath} written..\n";
    }

    $time_end = microtime(true);
    $execution_time = number_format($time_end - $time_start, 2);
    echo C__COLOR__LIGHT_GREEN . "\n\nDone. i-doit Api marshalled into " . C__COLOR__WHITE . "{$created_files}" . C__COLOR__LIGHT_GREEN . " files (in " . C__COLOR__WHITE . "{$execution_time}secs" . C__COLOR__LIGHT_GREEN . ")" . C__COLOR__LIGHT_GREEN . ". Enjoy.\n\n" . C__COLOR__NO_COLOR;
    exit;

} catch (idoit\Api\Exception $e) {
    echo $e->getMessage() . "\n";
}

/**
 * ------------------------------------------------------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------------------------------------------------------
 */
function progress()
{
    echo '.';
}

/**
 * @param string $categoryTitle
 */
function categoryDataHeader($categoryTitle, $categoryType = 'Global', $data, $const)
{
    global $phpHeader;

    $properties = implode(
        ', ', array_map(

        /**
         * @param string $element
         */
            function ($element) {
                return $element['key'];
            }, $data
        )
    );

    $propertiesArray = array();
    foreach ($data as $prop) {
        if ($prop['plugin_type'] == 'dialog_list') {
            $prop['type'] = 'array';
        }

        $propertiesArray[$prop['key']] = $prop['type'];
    }

    $propertiesDefinition = str_replace('  );', ');', str_replace('array (', 'array(', str_replace("\n", "\n      ", var_export($propertiesArray, true))));

    return $phpHeader . '

namespace idoit\Api\CMDB\Category\\$categoryType;

use idoit\Api\CMDB\Category\Data;
use idoit\Api\CMDB\Dialog;

/**
 * Category Data Class
 *
 * @properties $properties
 * @package    idoit\Api\CMDB
 */
class $categoryTitle
    extends Data
{

    /**
     * Property definition
     *
     * @var array
     */
    protected \$Properties = $propertiesDefinition;

    /**
     * Identification constant of category $categoryTitle
     *
     * @var string
     */
    protected \$Identifier = \'$const\';


';
}

/**
 * @param $data
 * @param string $categoryTitle
 * @param $constant
 *
 * @return string
 *
 * @internal
 * @private
 */
function categoryDataContent($data, $categoryTitle, $constant)
{

    $l_content = '';
    foreach ($data as $prop) {
        if (isset($prop['key'])) {
            $additional = '';
            $title = isset($prop['description']) ? $prop['description'] : $prop['key'];
            $keyReadable = str_replace(' ', '', ucwords(strtolower(str_replace('_', ' ', str_replace('id', ' id', $prop['key'])))));
            $key = $prop['key'];
            $typeHint = '';

            switch ($prop['type']) {
                case 'double':
                case 'float':
                    $type = 'float';
                    break;
                case 'int':
                    $type = 'int';

                    if ($keyReadable) {
                        $l_content .= '
    /**
     * Get property $title
     *
     * @key      $key
     *
     * @return   int
     * @public
     */
    public function getDialog$keyReadable(\$apiclient)
    {
        return Dialog::factory(\$apiclient)->get(\$this->Identifier, \'$key\');
    }
';
                        $l_content .= "\n\n";
                    }

                    break;
                default:
                    $type = 'string';
                    break;
            }

            if ($prop['plugin_type'] == 'dialog_list') {
                $type = 'array';
                $typeHint = 'array ';
                $defaultReturnVal = "array()";
            } else {
                $defaultReturnVal = "''";
            }

            $db_table = $prop['db_table'] ? "\n\t  * @db_alias " . $prop['db_table'] : '';
            $db_field = $prop['db_field'];

            if ($prop['objectTypeFilter']) {
                $additional .= "\n\t  * @objectTypeFilter " . $prop['objectTypeFilter'];
            }

            $l_content .= '
    /**
     * Get property $title
     *
     * @key      $key
     * @db_field $db_field$db_table
     *
     * @return   $type
     * @public$additional
     */
    public function get$keyReadable()
    {
        return isset(\$this->Content[\'$key\']) ? \$this->Content[\'$key\'] : $defaultReturnVal;
    }

    /**
     * Set property $title
     *
     * @param  $type \$value
     * @key    $key
     *
     * @return $categoryTitle
     * @public
     */
    public function set$keyReadable($typeHint\$value)
    {
        \$this->Content[\'$key\'] = $value;
        return \$this;
    }
';
            $l_content .= "\n\n";
        }

    }

    return $l_content;
}

/**
 * @param $category
 * @param $type
 * @param $data
 * @param $constant
 *
 * @throws Exception
 *
 * @internal
 * @private
 */
function writeCategory($category, $type, $data, $constant)
{
    if (count($data) === 0) {
        return;
    }

    $category = str_replace(' ', '', ucwords(strtolower($category)));

    /**
     * Handle php internal keywords
     */
    switch (strtolower($category)) {
        case 'global':
            $category = 'General';
            break;
        case 'static':
        case 'protected':
        case 'final':
        case 'public':
        case 'implements':
        case 'extends':
        case 'function':
        case 'class':
        case 'interface':
        case 'abstract':
            return;
            break;
    }

    $categoryDataDirectory = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'Api' . DIRECTORY_SEPARATOR . 'CMDB' . DIRECTORY_SEPARATOR . 'Category' . DIRECTORY_SEPARATOR . $type . DIRECTORY_SEPARATOR;

    if (file_exists($categoryDataDirectory) && is_writable($categoryDataDirectory)) {
        $categoryFileContent =
            categoryDataHeader($category, $type, $data, $constant) .
            categoryDataContent($data, $category, $constant) .
            "\n}";

        if (!file_put_contents($categoryDataDirectory . $category . '.php', $categoryFileContent)) {
            throw new Exception('Could not create file ' . $categoryDataDirectory . $category . '.php');
        } else {
            global $created_files;
            $created_files++;

            //printf('Dynamic codeset %s.php written..' . "\n", $categoryDataDirectory . $categoryTitle);
        }

    } else {
        throw new \Exception(sprintf('Error: Directory %s does not exist or is not writable.', $categoryDataDirectory));
    }
}