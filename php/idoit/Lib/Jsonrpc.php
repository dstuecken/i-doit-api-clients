<?php
/**
 * i-doit PHP API Client
 *
 * Copyright (c) 2016 Dennis Stücken
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package   $Package$
 * @version   $Version$
 * @copyright Dennis Stücken
 * @author    Dennis Stücken <dstuecken@i-doit.com>
 * @license   http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 */

namespace idoit\Lib;

use idoit\Api\Exception;
use idoit\Api\Request;

class Jsonrpc
{

    /**
     * Debug state
     *
     * @var boolean
     */
    private $m_debug;

    /**
     * The server URL
     *
     * @var string
     */
    private $m_url;

    /**
     * The request id
     *
     * @var integer
     */
    private $id = null;

    /**
     * @var string
     */
    public $username = '';

    /**
     * @var string
     */
    public $password = '';

    /**
     * @var string
     */
    public $sessionId = '';

    /**
     * If true, notifications are performed instead of requests
     *
     * @var boolean
     */
    private $notification = false;

    /**
     * @var array
     */
    private $requestHeaders = array();

    /**
     * @return string
     */
    public function getURL()
    {
        return $this->m_url;
    }

    /**
     * @return array
     */
    public function getHeadersOfLastRequest()
    {
        return $this->requestHeaders;
    }

    /**
     * Change request ID
     *
     * @param $id
     *
     * @return $this
     */
    public function setID($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @param string $sessionId
     */
    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;

        return $this;
    }

    /**
     * @param string $username
     *
     * @return $this
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Sets the notification state of the object. In this state, notifications are performed, instead of requests.
     *
     * @param boolean $notification
     */
    public function setRPCNotification($notification)
    {
        empty($notification) ?
            $this->notification = false
            :
            $this->notification = true;
    }

    /**
     * Create an api request
     *
     * @param string $method
     * @param mixed  $params
     *
     * @return array
     */
    public function request($method, $params)
    {
        return $this->__call($method, $params);
    }

    /**
     * Process a batch request
     *
     * @param Request[] $requests
     */
    public function batchRequest($requests)
    {
        $return = array();
        $reqs   = array();
        foreach ($requests as $request)
        {
            $reqs[] = $this->createRequest($request->getMethod(), $request->getParameters());
        }

        if (count($reqs) > 0)
        {
            $result = $this->sendRequest(json_encode($reqs));
            foreach ($result as $r)
            {
                if (isset($r['result']))
                {
                    $return[] = $r['result'];
                } else
                {
                    $return[] = $r;
                }
            }

        }

        return $return;
    }

    /**
     * @param string $method
     * @param $params
     *
     * @return string
     * @throws \idoit\Api\Exception
     */
    public function createRequest($method, $params)
    {
        // check
        if (!is_scalar($method))
        {
            throw new \Exception('Method name has no scalar value');
        }

        // check
        if (!is_array($params))
        {
            throw new \Exception('Params must be given as array');
        }

        // sets notification or prepare task
        if ($this->notification)
        {
            $currentId = NULL;
        } else
        {
            $currentId = ++$this->id;
        }

        // prepares the prepare
        $request = array(
            'method'  => $method,
            'params'  => $params,
            'id'      => $currentId,
            'version' => '2.0'
        );

        return $request;
    }

    /**
     * Performs a jsonRCP request; Returns results as array
     *
     * @param string $method
     * @param array  $params
     *
     * @return array
     */
    public function __call($method, $params)
    {
        $response = $this->sendRequest(json_encode($this->createRequest($method, $params)));

        // final checks and return
        if (!$this->notification)
        {
            // check
            /*if ($response['id'] != $this->id)
            {
                throw new Exception('Incorrect response id (request id: ' . $this->id . ', response id: ' . $response['id'] . ')', $response);
            }*/
            if (isset($response['error']) && !is_null($response['error']))
            {
                throw new Exception('Request error: ' . $response['error']['message'] . ' (' . @$response['error']['data']['error'] . ')', $response);
            }

            return $response['result'];

        } else
        {
            return true;
        }
    }

    /**
     * @param string $request (json request string)
     *
     * @return array|string
     * @throws \idoit\Api\Exception
     */
    public function sendRequest($request)
    {
        $this->m_debug && $this->m_debug .= '***** Request *****' . "\n" . $request . "\n" . '***** End Of request *****' . "\n\n";

        $header = array(
            'Content-type: application/json'
        );

        if ($this->username != '')
        {
            $header[] = 'Authorization: Basic ' . base64_encode($this->username . ':' . $this->password);
        }

        if ($this->sessionId)
        {
            $header[] = 'X-RPC-Auth-Session: ' . $this->sessionId;
        }

        // perform the HTTP POST
        $opts = array(
            'http' => array(
                'method'  => 'POST',
                'header'  => implode("\r\n", $header),
                'content' => $request
            ),
            'ssl' => array(
                'verify_peer'   => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true,
                'ciphers' => 'ALL:!AES:!3DES:!RC4:@STRENGTH', // OK:LOW
            )
        );
        $context = stream_context_create($opts);
        if ($fp = fopen($this->m_url, 'r', false, $context))
        {
            $response = '';
            while ($row = fgets($fp))
            {
                $response .= trim($row) . "\n";
            }

            $this->m_debug && $this->m_debug .= '***** Server response *****' . "\n" . $response . '***** End of server response *****' . "\n";
            $response = json_decode($response, true);

            if (isset($http_response_header) && is_array($http_response_header))
            {
                foreach ($http_response_header as $header)
                {
                    $tmp = explode(': ', $header);
                    if (isset($tmp[1]))
                    {
                        $this->requestHeaders[$tmp[0]] = $tmp[1];
                    }
                }
            }
        } else
        {
            throw new Exception('Unable to connect to ' . $this->m_url, array());
        }

        // debug output
        if ($this->m_debug)
        {
            echo nl2br($this->m_debug);
        }

        return $response;
    }

    /**
     * Takes the connection parameters
     *
     * @param string  $url
     * @param boolean $debug
     */
    public function __construct($url, $username = '', $password = '', $debug = false)
    {
        // server URL
        $this->m_url = $url;

        // authentification
        $this->username = $username;
        $this->password = $password;

        // proxy
        empty($proxy) ? $this->proxy = '' : $this->proxy = $proxy;
        // debug state
        empty($debug) ? $this->m_debug = false : $this->m_debug = true;
        // message id
        $this->id = 0;
    }
}
