<?php
/**
 * i-doit PHP API Client
 *
 * Copyright (c) 2016 Dennis Stücken
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package   $Package$
 * @version   $Version$
 * @copyright Dennis Stücken
 * @author    Dennis Stücken <dstuecken@i-doit.com>
 * @license   http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 */
namespace idoit\Api;

use \idoit\Api\Client as ApiClient;

class Base implements ApiInterface
{
    /**
     * @var ApiClient
     */
    protected $apiclient;

    /**
     * Prepares an API Request. Really sends it with Request->send().
     *
     * @param string $p_method
     * @param array $p_params
     *
     * @return Request
     */
    public function prepare($p_method, $p_params)
    {
        /**
         * Retrieve selected language
         */
        $p_params['language'] = $this->apiclient->language->toString();

        /**
         * Log current method
         */
        $this->apiclient->log->message($p_method, Log::VERBOSE);
        $this->apiclient->log->message('with Parameters: ' . var_export($p_params, true), Log::DEBUG);


        /**
         * Prepare the request
         */
        return new Request($this->apiclient, $p_method, $p_params);
    }


    /**
     * Prepares an Batch/Bulk API Request. Really sends it with BatchRequest->send().
     *
     * @param string $p_method
     * @param array $p_params
     *
     * @return BatchRequest
     */
    public function prepareBatch($p_method, $p_params)
    {
        /**
         * Log current method
         */
        $this->apiclient->log->message($p_method, Log::VERBOSE);
        $this->apiclient->log->message('with Parameters: ' . var_export($p_params, true), Log::DEBUG);

        /**
         * Prepare the request
         */
        return new BatchRequest(
            array(
                new Request(
                    $this->apiclient, $p_method, $p_params
                )
            )
        );
    }

    /**
     */
    public function __construct(ApiClient $apiclient)
    {
        $this->apiclient = $apiclient;
    }
}