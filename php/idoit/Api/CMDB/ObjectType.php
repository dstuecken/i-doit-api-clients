<?php
/**
 * i-doit PHP API Client
 *
 * Copyright (c) 2016 Dennis Stücken
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package   $Package$
 * @version   $Version$
 * @copyright Dennis Stücken
 * @author    Dennis Stücken <dstuecken@i-doit.com>
 * @license   http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 */
namespace idoit\Api\CMDB;

/**
 * Namespace alias
 */
use idoit\Api\Base;

/**
 * Class ObjectType
 * @package idoit\Api\CMDB
 */
class ObjectType
	extends Base
{

	/**
	 * Get All object types
	 *
	 * @return array
	 */
	public function getAll()
	{
		return $this->search(null, null, true);
	}

	/**
	 * Return specific object type by id or string constant (@see ObjectTypeConstants)
	 *
	 * @param $p_objectTypeID
	 *
	 * @return array
	 */
	public function get($p_objectTypeID)
	{
		$objectType = $this->prepare(
			Methods::ReadObjectTypes,
			array(
			     'filter' => array(
			        'id' => $p_objectTypeID
			     )
			)
		)->send();

		if ($objectType && isset($objectType[0]))
		{
			return $objectType[0];
		} else {
		    return array();
		}
	}

	/**
	 * Get All object types by group
	 *
	 * Possible object type groups:
	 *
	 *  define('C__OBJTYPE_GROUP__SOFTWARE',       1);
	 *	define('C__OBJTYPE_GROUP__INFRASTRUCTURE', 2);
	 *	define('C__OBJTYPE_GROUP__OTHER',          3);
	 *	define('C__OBJTYPE_GROUP__CONTACT',        1000);
	 *
	 * or simply retrieve them via \idoit\Api\CMDB\ObjectTypeGroup::getAll();
	 *
	 * @return array
	 */
	public function getAllByGroup($p_objectTypeGroup)
	{
		return $this->search($p_objectTypeGroup);
	}

	/**
	 * Retrieve assigned categories by object type
	 *
	 * @param $p_objectType
	 *
	 * @return array
	 */
	public function getAssignedCategories($p_objectType)
	{
		return $this->prepare(
			Methods::ReadObjectTypeCategories,
			array(
			     'type' => $p_objectType
			)
		)->send();
	}

	/**
	 * Search for object types
	 *
	 * @param int $p_typeGroup
	 * @param string $p_title
	 *
	 * @return array
	 */
	public function search($p_typeGroup = null, $p_title = null, $p_countObjects = true)
	{
		$l_filter = array();
		if ($p_title)
		{
			$l_filter['title'] = '%' . $p_title . '%';
		}

		if ($p_typeGroup)
		{
			$l_filter['type_group'] = $p_typeGroup;
		}

		return $this->prepare(
			Methods::ReadObjectTypes,
			array(
				 'countobjects' => $p_countObjects,
			     'filter'       => $l_filter
			)
		)->send();
	}

}
