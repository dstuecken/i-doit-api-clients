<?php
/**
 * i-doit PHP API Client
 *
 * Copyright (c) 2016 Dennis Stücken
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package   $Package$
 * @version   $Version$
 * @copyright Dennis Stücken
 * @author    Dennis Stücken <dstuecken@i-doit.com>
 * @license   http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 */
namespace idoit\Api\CMDB;

/**
 * Namespace alias
 */
use idoit\Api\Base;

/**
 * Class ObjectType
 * @package idoit\Api\CMDB
 */
class ObjectTypeGroup
    extends Base
{

    /**
     * Get All object type groups
     *
     * @return array
     */
    public function getAll()
    {
        return $this->search(NULL);
    }

    /**
     * Search or retrieve all object type groups
     *
     * @param string $p_title
     *
     * @return array
     */
    public function search($p_title)
    {
        $l_filter = array();
        if ($p_title)
        {
            $l_filter['title'] = '%' . $p_title . '%';
        }

        return $this->prepare(
            Methods::ReadObjectTypeGroups,
            array(
                    'filter' => $l_filter
            )
        )->send();
    }

}
