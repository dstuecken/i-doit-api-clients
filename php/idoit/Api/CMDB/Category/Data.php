<?php
/**
 * i-doit PHP API Client
 *
 * Copyright (c) 2016 Dennis Stücken
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package   $Package$
 * @version   $Version$
 * @copyright Dennis Stücken
 * @author    Dennis Stücken <dstuecken@i-doit.com>
 * @license   http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 */

/**
 * Namespace declaration
 */
namespace idoit\Api\CMDB\Category;

/**
 * Class Category
 *
 * @package idoit\Api\CMDB
 */
class Data
implements \IteratorAggregate, \ArrayAccess, \Countable
{

    /**
     * Property definition
     *
     * @var array
     */
    protected $Properties = array();

    /**
     * Data content
     *
     * @var array
     */
    protected $Content = array();

    /**
     * Category Identifer, usually the cateogry constant
     *
     * @var string
     */
    protected $Identifier = '';

    /**
     * Data factory
     *
     * @return static
     */
    public static function factory()
    {
        return new static();
    }

    /**
     * @return array
     */
    public function getProperties()
    {
        return $this->Properties;
    }

    /**
     * The category's identifier (usually a string with the constants name of the category; e.g. C__CATG__CPU)
     *
     * @return string
     */
    public function getCategoryIdentifier()
    {
        return $this->Identifier;
    }

    /**
     * Initializes an empty dataset with use of the categories properties
     *
     * @return $this
     */
    public function initializeEmpty()
    {
        foreach ($this->Properties as $key => $type)
        {
            $this->Content[$key] = '';
        }

        return $this;
    }

    /**
     * @param $p_dataArray
     */
    public function setData($p_dataArray)
    {
        $this->Content = $p_dataArray;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->Content;
    }

    /**
     * @return string
     */
    public function toJSON()
    {
        return json_encode($this->Content /*, JSON_OBJECT_AS_ARRAY*/);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return http_build_url($this->Content);
    }

    /* ArrayIterator methods -------------------------------------------------------------- */

    /**
     * Implementation of IteratorAggregate::getIterator()
     *
     * @return array iterator object for looping
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->Content);
    }

    /**
     * Implementation of ArrayAccess:offsetSet()
     *
     * isys_tree_collection[$p_key] = "foobar";
     *
     * @param mixed $p_key
     * @param mixed $value
     */
    public function offsetSet($p_key, $value)
    {
        $this->Content[$p_key] = $value;
    }

    /**
     * Implementation of ArrayAccess:offsetGet()
     *
     * isys_tree_collection[$p_key];
     *
     * @param $p_key
     *
     * @return mixed
     */
    public function offsetGet($p_key)
    {
        return $this->Content[$p_key];
    }

    /**
     * Implementation of ArrayAccess:offsetUnset()
     *
     * unset(isys_tree_collection);
     *
     * @param $p_key
     */
    public function offsetUnset($p_key)
    {
        unset($this->Content[$p_key]);
    }

    /**
     * Implementation of ArrayAccess:offsetExists()
     *
     * isset(isys_tree_collection);
     *
     * @param  $p_key
     *
     * @return bool
     */
    public function offsetExists($p_key)
    {
        return isset($this->Content[$p_key]);
    }

    /**
     * Implementation of ArrayAccess:indexOf()
     *
     * @param $node
     *
     * @return int|null|string
     */
    public function indexOf($node)
    {
        foreach ($this->Content as $i => $_node) if ($node === $_node) return $i;

        return NULL;
    }

    /**
     * Implementation of ArrayAccess:count()
     *
     * @return int
     */
    public function count()
    {
        return count($this->Content);
    }
}
