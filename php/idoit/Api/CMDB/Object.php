<?php
/**
 * i-doit PHP API Client
 *
 * Copyright (c) 2016 Dennis Stücken
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package   $Package$
 * @version   $Version$
 * @copyright Dennis Stücken
 * @author    Dennis Stücken <dstuecken@i-doit.com>
 * @license   http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 */
namespace idoit\Api\CMDB;

/**
 * Namespace alias
 */
use idoit\Api\Exception;
use idoit\Api\Base;

/**
 * Class Object
 * @package idoit\Api\CMDB
 */
class Object
	extends Base
{
	/**
	 * Search for an object by title and type
	 *
	 * @param string $p_title
	 * @param string $p_type
	 * @return mixed
	 */
	public function search($p_title, $p_type = null)
	{
		$l_filter = array();

		if ($p_title)
		{
			$l_filter = array(
				'title' => $p_title
			);
		}

		if ($p_type)
		{
			$l_filter['type'] = $p_type;
		}

		return $this->prepare(
			Methods::ReadObjects,
			array(
			     'filter' => $l_filter
			)
		)->send();
	}

	/**
	 * Retrieve an object by ID
	 *
	 * @return mixed
	 */
	public function get($p_id)
	{
		$object = $this->prepare(
			Methods::ReadObject,
			array(
				'id' => $p_id
			)
		)->send();

		if (isset($object[0]))
		{
			return $object[0];
		}

		return $object;
	}

	/**
	 * Retrieve all objects by object type id or constant
	 *
	 * Use ObjectTypeConstants for a list of possible object types
	 *
	 * @param $p_objecType
	 *
	 * @return mixed
	 */
	public function getAllByType($p_objecType)
	{
		return $this->search(null, $p_objecType);
	}

	/**
	 * @param $p_id
	 *
	 * @return string
	 * @throws \idoit\Api\Exception
	 */
	public function getTitleById($p_id)
	{
		$return = $this->prepare(
			Methods::ReadObject,
			array(
				'id' => $p_id
			)
		)->send();

		if ($return)
		{
			return
				isset($return['title']) ?
					  $return['title'] : (isset($return[0]['title']) ?
		              $return[0]['title'] : '');
		}

		throw new Exception(sprintf('Object with id %s was not found', $p_id));
	}

	/**
	 * Retrieve assigned categories by object
	 *
	 * @param $p_objectType
	 *
	 * @return array
	 */
	public function getAssignedCategories($p_objectID)
	{
		if ($p_objectID > 0)
		{
			$object = $this->get($p_objectID);

			if ($object && isset($object['objecttype']))
			{
				return $this->prepare(
					Methods::ReadObjectTypeCategories,
					array(
					     'type' => $object['objecttype']
					)
				)->send();
			}
		}

		return array();
	}

	/**
	 * Search for an object by title and type
	 *
	 * @param string $p_title
	 * @param string $p_type
	 * @return mixed
	 */
	public function getIdByTitle($p_title, $p_type = null)
	{
		$l_filter = array(
			'title' => $p_title
		);

		if ($p_type)
		{
			$l_filter['type'] = $p_type;
		}

		$l_objects = $this->prepare(
			Methods::ReadObjects,
			array(
			     'filter' => $l_filter
			)
		)->send();

		return isset($l_objects[0]['id']) ? $l_objects[0]['id'] : null;
	}

	/**
	 * Search for an object by title and type
	 *
	 * @param string $p_title
	 * @param string $p_type
	 * @return mixed
	 */
	public function delete($p_title, $p_type = null)
	{
		return $this->prepare(
			Methods::ReadObjects,
			array(
			     'filter' => array(
				     'title' => $p_title,
				     'type'  => $p_type
			     )
			)
		)->send();
	}

	/**
	 * Changes the location of $p_objectID to $p_locationID. Both have to be object ids!
	 *
	 * @param $p_objectID
	 * @param $p_locationID
	 *
	 * @return array
	 */
	public function updateLocation($p_objectID, $p_locationID)
	{
		return $this->prepare(
			Methods::UpdateCategory,
			array(
			     'catgID' => CategoryConstants::G_LOCATION,
			     'objID'  => $p_objectID,
			     'data'   => array(
				     'parent' => $p_locationID
			     )
			)
		)->send();
	}

	/**
	 * Creates an object by title $p_title and type $p_objectType
	 *
	 * @param $p_title
	 * @param $p_objectType
	 *
	 * @return array
	 */
	public function createObject($p_title, $p_objectType)
	{
		if (!$p_title)
		{
			throw new Exception('You have to specify an object title for creating an object.');
		}

		if (!$p_objectType)
		{
			throw new Exception('You have to specify an object type for creating an object.');
		}

		return $this->prepare(
			Methods::CreateObject,
			array(
			     'title' => $p_title,
			     'type'  => $p_objectType
			)
		)->send();
	}

	/**
	 * - Creates an object of type $p_objectType if object was not found and returns it's new ID.
	 * - Returns the object ID when an object with title $p_title and object type $p_objectType was found.
	 *
	 * @param      $p_title
	 * @param null $p_objectType
	 *
	 * @return mixed
	 * @throws \idoit\Api\Exception
	 */
	public function createOrGetObjectId($p_title, $p_objectType = null)
	{
		if (!$p_objectType)
		{
			if (defined('\idoit\Api\CMDB\ObjectTypeConstants::TYPE_SERVER'))
			{
				$p_objectType = \idoit\Api\CMDB\ObjectTypeConstants::TYPE_SERVER;
			}
		}

		if (!$p_objectType)
		{
			throw new Exception('You have to specify an object type for creating or searching an object by title.');
		}

		// Checking for existing object
		$l_testObject = $this->search($p_title, $p_objectType);

		// Object does not exist, so create it
		if (count($l_testObject) === 0)
		{
			$l_createObject = $this->createObject($p_title, $p_objectType);

			if ($l_createObject['success'])
			{
				return $l_createObject['id'];
			} else {
			    throw new ApiException('Error while creating object: ' . (isset($l_createObject['error']) ? $l_createObject['error'] : 'Unexpected API response'));
			}
		} else
		{
			if (isset($l_testObject[0]))
			{
				return $l_testObject[0]['id'];
			} else {
			    throw new Exception('Error retrieving object id: Retrieved an unexpected result from API.');
			}
		}
	}

}