<?php
/**
 * i-doit PHP API Client
 *
 * Copyright (c) 2016 Dennis Stücken
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package   $Package$
 * @version   $Version$
 * @copyright Dennis Stücken
 * @author    Dennis Stücken <dstuecken@i-doit.com>
 * @license   http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 */

namespace idoit\Api\CMDB;

class Methods extends \idoit\Api\Methods
{

    /* Object methods */
    const ReadObject   = 'cmdb.object.read';
    const CreateObject = 'cmdb.object.create';
    const UpdateObject = 'cmdb.object.update';
    const DeleteObject = 'cmdb.object.delete';

    const ReadObjects   = 'cmdb.objects.read';
    const DeleteObjects = 'cmdb.objects.delete';

    /* Object type methods */
    const ReadObjectTypes          = 'cmdb.object_types.read';
    const ReadObjectTypeGroups     = 'cmdb.object_type_groups.read';
    const ReadObjectTypeCategories = 'cmdb.object_type_categories.read';

    /* Category methods */
    const ReadCategory   = 'cmdb.category.read';
    const CreateCategory = 'cmdb.category.create';
    const UpdateCategory = 'cmdb.category.update';
    const DeleteCategory = 'cmdb.category.delete';

    /* Category structure */
    const ReadCategoryStructure = 'cmdb.category_info.read';

    /* Dialog methods */
    const ReadDialog = 'cmdb.dialog.read';
    const CreateDialog = 'cmdb.dialog.create';
    const UpdateDialog = 'cmdb.dialog.update';
    const DeleteDialog = 'cmdb.dialog.delete';

}
