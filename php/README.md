#i-doit API Client Library for PHP

The i-doit API Client is a library targetting PHP to provide an easy way to interact with the i-doit JSON-RPC API.

## Install / Initalize

In order to use the direct programming interface, you need to initialize the api client with your i-doit installation.
This process creates a dynamic codeset to directly access object type constants, category constants and category methods, depending on your i-doit database.
You will even be able to access custom created object types.

Make sure you have an **i-doit with activated API and [make](http://de.wikipedia.org/wiki/Make) installed** first, since it is needed prior to initializing the code:

 * Windows users should take a look at gnuwin32, mingw or cygwin in order to experience the joy of makefiles and the make tools.
 * Mac OS X users just need to have XCode and the command line tools installed with 'xcode-select --install'.
 * Linux users can normally lay back and relax. If not, ask your package manager about make.

Once this has been ensured, initialize the api client with the following command in the client directory (this is where Makefile is located..):

```
make initialize
```

*All the people who do not want to deal with installing make or having problems with it (I hope you will not). You could also directly call the php inizializer:*

```
php idoit/Lib/Initialize.php
```


## Usage

**include the APi Client Interface**

by using the unpacked source code

```
<?php
include_once('apiclient.php');
```

or from Phar Archive (phar archive unfortunately not available, yet):

```
<?php
include_once('phar://idoit-api-client.phar/apiclient.php');
```

**Initialize the Api Client and create a connection instance**

```
<?php
$apiClient = new idoit\Api\Client(
	new idoit\Api\Connection(
		'http://demo.i-doit.com/src/jsonrpc.php', 'c1ia5q'
	)
);
```

**Send a request with your apiclient Instance**

```
<?php
$request = new \idoit\Api\Request($apiClient, \idoit\Api\Methods::IdoitVersion);
```

**Call a function provided by the api libray, ID by a specific title in this case**

```
<?php
$objectApi = new idoit\Api\CMDB\Object($apiClient);
$objectID  = $objectApi->getIdByTitle('ACC', 'C__OBJTYPE__SERVER');
```


**Create a new object using the api library**


```
<?php
$objectApi = new idoit\Api\CMDB\Object($apiClient);
$objectID  = $objectApi->createObject('CONTRACT01', 'C__OBJTYPE__MAINTENANCE');
```


**Call a function using the direct programming interface**

```
<?php
$objectApi = new idoit\Api\CMDB\Object($apiClient);
$objectID  = $objectApi->getIdByTitle('CONTRACT01', idoit\Api\CMDB\ObjectTypeConstants::TYPE_MAINTENANCE);

if (!$objectID) throw new Exception('Object with Name CONTRACT01 was not found :-(')

$categoryData = new \idoit\Api\CMDB\Category\S\Contract();
$categoryData->setData(
	array(
		'type'                    => '',
		'contract_no'             => 'text',
		'customer_no'             => 'text',
		'internal_no'             => 'text',
		'costs'                   => 'double',
		'product'                 => 'text',
		'reaction_rate'           => '',
		'contract_status'         => '',
		'start_date'              => '01.01.1999',
		'end_date'                => '01.01.2015',
		'run_time'                => '',
		'run_time_unit'           => '',
		'next_contract_end_date'  => '01.01.2015',
		'end_type'                => '',
		'next_notice_end_date'    => '01.01.2015',
		'notice_date'             => '01.01.2015',
		'notice_period'           => '',
		'notice_period_unit'      => '',
		'notice_type'             => '',
		'maintenance_period'      => '',
		'maintenance_period_unit' => '',
		'payment_period'          => '',
		'description'             => 'test description'
	)
)->setDescription('oh that was wrong, better change description to this!');

// Add a new Contract Category entry to object with ID $objectID of name 'CONTRACT01'
print_r(
	$l_category->add($objectID, $categoryData)
);
```

## Examples

See examples/ for usage examples.