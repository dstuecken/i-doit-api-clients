/*
 * The MIT License
 *
 * Copyright 2016 Thomas Wittek <thomas.wittek@shd-online.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.idoit.examples;

import com.idoit.api.IdoitClient;
import com.idoit.api.IdoitClientException;
import com.idoit.api.cmdb.CMDBObject;
import com.idoit.api.cmdb.ObjectTypeConstants;
import com.idoit.api.cmdb.category.CMDBCategory;
import com.idoit.api.cmdb.category.CategoryConstants;
import java.util.Collection;
import java.util.Set;
import org.json.JSONException;

/**
 *
 * @author Thomas Wittek <thomas.wittek@shd-online.de>
 */
public class Example06_Category {

    public static void main(String[] args) throws JSONException {
        try {
            // URL und API-KEY in der Configuration Datei anpassen!
            // Client erstellen
            IdoitClient client = new IdoitClient(Configuration.IDOIT_URL, Configuration.API_KEY);

            // Login
            client.login("admin", "admin");

            // Objekt anlegen
            CMDBObject cmdbObject = client.object().create(ObjectTypeConstants.TYPE_SERVER, "Test-Server");

            // Categorie lesen
            Collection<CMDBCategory> category = client.category().read(cmdbObject.getID(), CategoryConstants.G.CPU);

            // alle g Categories lesen
            Set<String> keys = client.constants().categories().g().keySet();
            for (String key : keys) {
                Collection<CMDBCategory> categoryG = client.category().read(cmdbObject.getID(), key);
            }

            // alle s Categories lessen
            keys = client.constants().categories().s().keySet();
            for (String key : keys) {
                Collection<CMDBCategory> categoryS = client.category().read(cmdbObject.getID(), key);
            }

            // Category anlegen -> Werte bitte entsprechend anpassen
            CMDBCategory newCategory = new CMDBCategory();
            newCategory.createValue("title").setTitle("CPU");
            newCategory.createValue("manufacturer").putProperty("id", "2");
            newCategory.createValue("type").putProperty("id", "11");

            CMDBCategory createdCategory = client.category().create(cmdbObject.getID(), CategoryConstants.G.CPU, newCategory);

            // Category Eintrag über entryID lesen
            CMDBCategory createdCategory2 = client.category().read(cmdbObject.getID(), CategoryConstants.G.CPU, createdCategory.getID());
            
            // Category updaten
            CMDBCategory updateCategory = new CMDBCategory();
            updateCategory.createValue("id").setTitle(createdCategory.getID());
            updateCategory.createValue("title").setTitle("CPU updated");
            CMDBCategory updatedCategory = client.category().update(cmdbObject.getID(), CategoryConstants.G.CPU, updateCategory);

            // Category löschen
            boolean categoryDelete = client.category().delete(cmdbObject.getID(), CategoryConstants.G.CPU, createdCategory.getID());
            
            // Object löschen
            boolean objectDelete = client.object().delete(cmdbObject.getID());

            // Logout
            client.logout();

        } catch (IdoitClientException ex) {
            System.out.println("RPC-ERROR");
            System.out.println("---------");
            System.out.println("message: " + ex.getMessage());
            System.out.println("error: " + ex.getError());
            System.out.println("code: " + ex.getCode());
        }
    }
}
