/*
 * The MIT License
 *
 * Copyright 2016 Thomas Wittek <thomas.wittek@shd-online.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.idoit.api;

import com.idoit.api.cmdb.CMDBObjectType;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Thomas Wittek <thomas.wittek@shd-online.de>
 */
public class ObjectTypeModule implements MethodConstants {

    private final IdoitClient client;
    private final static String ORDER_BY_ID = "id";
    private final static String ORDER_BY_TITLE = "title";
    private final static String ORDER_BY_STATUS = "status";

    private String orderBy = "";
    private int pageSize = 0;
    private int page = 0;
    private String sort = "asc";
    private boolean countObjects = false;

    public ObjectTypeModule(IdoitClient client) {
        this.client = client;
    }

    /**
     * Read the object types.
     *
     * @see CMDBObjectType
     * @return Collection of all object types
     * @throws IdoitClientException
     */
    public List<CMDBObjectType> read() throws IdoitClientException {
        JSONObject responseObject = client.sendRequest(CMDB_OBJECT_TYPES, createParams(), "1");
        return createCMDBObjectTypes(responseObject);
    }

    /**
     * Set the page size for object types query.
     *
     * @param pageSize the page size
     * @return ObjectTypeModule
     */
    public ObjectTypeModule pageSize(int pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    /**
     * Set the number of the page for object types query.
     *
     * @param page the page number
     * @return ObjectTypeModule
     */
    public ObjectTypeModule page(int page) {
        this.page = page;
        return this;
    }

    /**
     * Order the object types by ID.
     *
     * @return ObjectTypeModule
     */
    public ObjectTypeModule orderByID() {
        this.orderBy = ORDER_BY_ID;
        return this;
    }

    /**
     * Order the object types by title.
     *
     * @return ObjectTypeModule
     */
    public ObjectTypeModule orderByTitle() {
        this.orderBy = ORDER_BY_TITLE;
        return this;
    }

    /**
     * Order the object types by status.
     *
     * @return ObjectTypeModule
     */
    public ObjectTypeModule orderByStatus() {
        this.orderBy = ORDER_BY_STATUS;
        return this;
    }

    /**
     * Sort the object types ascending.
     *
     * @return ObjectTypeModule
     */
    public ObjectTypeModule sortAscending() {
        this.sort = "asc";
        return this;
    }

    /**
     * Sort the object types descending.
     *
     * @return ObjectTypeModule
     */
    public ObjectTypeModule sortDescending() {
        this.sort = "desc";
        return this;
    }

    /**
     * Counts for each returned object types whose documented objects from the
     * status "Normal"
     *
     * @return ObjectTypeModule
     */
    public ObjectTypeModule countObjects() {
        this.countObjects = true;
        return this;
    }

    private JSONObject createParams() throws IdoitClientException {
        try {
            JSONObject params = new JSONObject();
            if (!orderBy.isEmpty()) {
                params.put("order_by", orderBy);
            }
            if (pageSize > 0) {
                int offset = page * pageSize;
                params.put("limit", String.format("%d,%d", offset, pageSize));
            }
            if (countObjects) {
                params.put("countobjects", true);
            }
            params.put("sort", sort);
            return params;
        } catch (JSONException ex) {
            throw new IdoitClientException(ex.getLocalizedMessage(), ex.getCause());
        }
    }

    private List<CMDBObjectType> createCMDBObjectTypes(JSONObject jsonObject) throws IdoitClientException {
        try {
            List<CMDBObjectType> objectTypes = new ArrayList<>();
            JSONArray resultObjects = jsonObject.getJSONArray("result");

            for (int i = 0; i < resultObjects.length(); i++) {
                JSONObject resultObject = resultObjects.getJSONObject(i);
                CMDBObjectType objectType = new CMDBObjectType();
                objectType.setID(resultObject.optString("id", ""));
                objectType.setTitle(resultObject.optString("title", ""));
                objectType.setContainer(resultObject.optString("container", ""));
                objectType.setConstant(resultObject.optString("const", ""));
                objectType.setColorCode(resultObject.optString("color", ""));
                objectType.setImageUrl(resultObject.optString("image", ""));
                objectType.setIconPath(resultObject.optString("icon", ""));
                objectType.setCats(resultObject.optString("cats", ""));
                objectType.setTreeGroup(resultObject.optString("tree_group", ""));
                objectType.setStatus(resultObject.optString("status", ""));
                objectType.setTypeGroup(resultObject.optString("type_group", ""));
                objectType.setTypeGroupTitle(resultObject.optString("type_group_title", ""));
                objectType.setObjectCount(Integer.parseInt(resultObject.optString("objectcount", "-1")));
                objectTypes.add(objectType);
            }
            return objectTypes;
        } catch (JSONException ex) {
            throw new IdoitClientException(ex.getLocalizedMessage(), jsonObject.toString());
        }
    }
}
