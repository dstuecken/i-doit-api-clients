/*
 * The MIT License
 *
 * Copyright 2016 Thomas Wittek <thomas.wittek@shd-online.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.idoit.api.cmdb.category;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Thomas Wittek <thomas.wittek@shd-online.de>
 */
public class CategoryAttribute {

    private String title;
    private Info info;
    private Data data;
    private Ui ui;
    private List callback;
    private boolean mandatory;

    public String getTitle() {
        if (title == null) {
            title = "";
        }
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List getCallback() {
        if (callback == null) {
            callback = new ArrayList();
        }
        return callback;
    }

    public void setCallback(List callback) {
        this.callback = callback;
    }

    public boolean isMandatory() {
        return mandatory;
    }

    public void setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
    }

    public Info getInfo() {
        if (info == null) {
            info = new Info();
        }
        return info;
    }

    public Data getData() {
        if (data == null) {
            data = new Data();
        }
        return data;
    }

    public Ui getUi() {
        if (ui == null) {
            ui = new Ui();
        }
        return ui;
    }

    public class Info {

        private boolean primaryField;
        private String type;
        private boolean backward;
        private String title;
        private String description;

        public boolean isPrimaryField() {
            return primaryField;
        }

        public void setPrimaryField(boolean primaryField) {
            this.primaryField = primaryField;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public boolean isBackward() {
            return backward;
        }

        public void setBackward(boolean backward) {
            this.backward = backward;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

    }

    public class Data {

        private String type;
        private boolean readonly;
        private String field;
        private List<String> references;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public boolean isReadonly() {
            return readonly;
        }

        public void setReadonly(boolean readonly) {
            this.readonly = readonly;
        }

        public String getField() {
            return field;
        }

        public void setField(String field) {
            this.field = field;
        }

        public List<String> getReferences() {
            if (references == null) {
                references = new ArrayList<>();
            }
            return references;
        }

        public void setReferences(List<String> references) {
            this.references = references;
        }

    }

    public class Ui {

        private String type;
        private Map<String, Object> params;
        private Object defaultValue;
        private String id;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public Map<String, Object> getParams() {
            if (params == null) {
                params = new HashMap<>();
            }
            return params;
        }

        public void setParams(Map<String, Object> params) {
            this.params = params;
        }

        public Object getDefaultValue() {
            return defaultValue;
        }

        public void setDefaultValue(Object defaultValue) {
            this.defaultValue = defaultValue;
        }

        public String getID() {
            return id;
        }

        public void setID(String id) {
            this.id = id;
        }
    }
}
