/*
 * The MIT License
 *
 * Copyright 2016 Thomas Wittek <thomas.wittek@shd-online.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.idoit.api.cmdb.category;

/**
 *
 * @author Thomas Wittek <thomas.wittek@shd-online.de>
 */
public interface CategoryConstants {

    public interface G {

        /**
         * General
         */
        public final static String GLOBAL = "C__CATG__GLOBAL";

        /**
         * Model
         */
        public final static String MODEL = "C__CATG__MODEL";

        /**
         * Form Factor
         */
        public final static String FORMFACTOR = "C__CATG__FORMFACTOR";

        /**
         * Cpu
         */
        public final static String CPU = "C__CATG__CPU";

        /**
         * Memory
         */
        public final static String MEMORY = "C__CATG__MEMORY";

        /**
         * Network
         */
        public final static String NETWORK = "C__CATG__NETWORK";

        /**
         * Direct Attached Storage
         */
        public final static String STORAGE = "C__CATG__STORAGE";

        /**
         * Power Consumer
         */
        public final static String POWER_CONSUMER = "C__CATG__POWER_CONSUMER";

        /**
         * Interface
         */
        public final static String UNIVERSAL_INTERFACE = "C__CATG__UNIVERSAL_INTERFACE";

        /**
         * Software Assignment
         */
        public final static String APPLICATION = "C__CATG__APPLICATION";

        /**
         * Access
         */
        public final static String ACCESS = "C__CATG__ACCESS";

        /**
         * Backup
         */
        public final static String BACKUP = "C__CATG__BACKUP";

        /**
         * Emergency Plan Assignment
         */
        public final static String EMERGENCY_PLAN = "C__CATG__EMERGENCY_PLAN";

        /**
         * Files
         */
        public final static String FILE = "C__CATG__FILE";

        /**
         * Contact Assignment
         */
        public final static String CONTACT = "C__CATG__CONTACT";

        /**
         * Logbook
         */
        public final static String LOGBOOK = "C__CATG__LOGBOOK";

        /**
         * Controller
         */
        public final static String CONTROLLER = "C__CATG__CONTROLLER";

        /**
         * Location
         */
        public final static String LOCATION = "C__CATG__LOCATION";

        /**
         * Object Picture
         */
        public final static String IMAGE = "C__CATG__IMAGE";

        /**
         * Manual Assignment
         */
        public final static String MANUAL = "C__CATG__MANUAL";

        /**
         * Overview Page
         */
        public final static String OVERVIEW = "C__CATG__OVERVIEW";

        /**
         * Workflows
         */
        public final static String WORKFLOW = "C__CATG__WORKFLOW";

        /**
         * Sound Card
         */
        public final static String SOUND = "C__CATG__SOUND";

        /**
         * Locally Assigned Objects
         */
        public final static String OBJECT = "C__CATG__OBJECT";

        /**
         * Graphic Card
         */
        public final static String GRAPHIC = "C__CATG__GRAPHIC";

        /**
         * Virtual Machine
         */
        public final static String VIRTUAL_MACHINE = "C__CATG__VIRTUAL_MACHINE";

        /**
         * Accounting
         */
        public final static String ACCOUNTING = "C__CATG__ACCOUNTING";

        /**
         * Port
         */
        public final static String SUBCAT_NETWORK_PORT = "C__CMDB__SUBCAT__NETWORK_PORT";

        /**
         * Interface
         */
        public final static String SUBCAT_NETWORK_INTERFACE_P = "C__CMDB__SUBCAT__NETWORK_INTERFACE_P";

        /**
         * Logical Ports
         */
        public final static String SUBCAT_NETWORK_INTERFACE_L = "C__CMDB__SUBCAT__NETWORK_INTERFACE_L";

        /**
         * Drive
         */
        public final static String DRIVE = "C__CATG__DRIVE";

        /**
         * Device
         */
        public final static String SUBCAT_STORAGE__DEVICE = "C__CMDB__SUBCAT__STORAGE__DEVICE";

        /**
         * Fc Port
         */
        public final static String CONTROLLER_FC_PORT = "C__CATG__CONTROLLER_FC_PORT";

        /**
         * Storage Area Network
         */
        public final static String SANPOOL = "C__CATG__SANPOOL";

        /**
         * Host Address
         */
        public final static String IP = "C__CATG__IP";

        /**
         * Version
         */
        public final static String VERSION = "C__CATG__VERSION";

        /**
         * Cabling
         */
        public final static String CABLING = "C__CATG__CABLING";

        /**
         * Connectors
         */
        public final static String CONNECTOR = "C__CATG__CONNECTOR";

        /**
         * Invoice
         */
        public final static String INVOICE = "C__CATG__INVOICE";

        /**
         * Tickets
         */
        public final static String TICKETS = "C__CATG__TICKETS";

        /**
         * Custom Fields
         */
        public final static String CUSTOM_FIELDS = "C__CATG__CUSTOM_FIELDS";

        /**
         * Power Supplier
         */
        public final static String POWER_SUPPLIER = "C__CATG__POWER_SUPPLIER";

        /**
         * Raid-array
         */
        public final static String RAID = "C__CATG__RAID";

        /**
         * Logical Devices (ldev Server)
         */
        public final static String LDEV_SERVER = "C__CATG__LDEV_SERVER";

        /**
         * Logical Devices (client)
         */
        public final static String LDEV_CLIENT = "C__CATG__LDEV_CLIENT";

        /**
         * Host Bus Adapter (hba)
         */
        public final static String HBA = "C__CATG__HBA";

        /**
         * Cluster
         */
        public final static String CLUSTER_ROOT = "C__CATG__CLUSTER_ROOT";

        /**
         * Cluster
         */
        public final static String CLUSTER = "C__CATG__CLUSTER";

        /**
         * Shares
         */
        public final static String SHARES = "C__CATG__SHARES";

        /**
         * Cluster Service Assignment
         */
        public final static String CLUSTER_SERVICE = "C__CATG__CLUSTER_SERVICE";

        /**
         * Cluster Members
         */
        public final static String CLUSTER_MEMBERS = "C__CATG__CLUSTER_MEMBERS";

        /**
         * Shared Storage
         */
        public final static String CLUSTER_SHARED_STORAGE = "C__CATG__CLUSTER_SHARED_STORAGE";

        /**
         * Cluster Memberships
         */
        public final static String CLUSTER_MEMBERSHIPS = "C__CATG__CLUSTER_MEMBERSHIPS";

        /**
         * Computing Resources
         */
        public final static String COMPUTING_RESOURCES = "C__CATG__COMPUTING_RESOURCES";

        /**
         * Cluster Vitality
         */
        public final static String CLUSTER_VITALITY = "C__CATG__CLUSTER_VITALITY";

        /**
         * Snmp
         */
        public final static String SNMP = "C__CATG__SNMP";

        /**
         * Virtual Host
         */
        public final static String VIRTUAL_HOST_ROOT = "C__CATG__VIRTUAL_HOST_ROOT";

        /**
         * Virtual Host
         */
        public final static String VIRTUAL_HOST = "C__CATG__VIRTUAL_HOST";

        /**
         * Guest Systems
         */
        public final static String GUEST_SYSTEMS = "C__CATG__GUEST_SYSTEMS";

        /**
         * Virtual Machine
         */
        public final static String VIRTUAL_MACHINE__ROOT = "C__CATG__VIRTUAL_MACHINE__ROOT";

        /**
         * Virtual Switches
         */
        public final static String VIRTUAL_SWITCH = "C__CATG__VIRTUAL_SWITCH";

        /**
         * Virtual Devices
         */
        public final static String VIRTUAL_DEVICE = "C__CATG__VIRTUAL_DEVICE";

        /**
         * Shared Virtual Switches
         */
        public final static String CLUSTER_SHARED_VIRTUAL_SWITCH = "C__CATG__CLUSTER_SHARED_VIRTUAL_SWITCH";

        /**
         * Backup (assigned Objects)
         */
        public final static String BACKUP__ASSIGNED_OBJECTS = "C__CATG__BACKUP__ASSIGNED_OBJECTS";

        /**
         * Group Memberships
         */
        public final static String GROUP_MEMBERSHIPS = "C__CATG__GROUP_MEMBERSHIPS";

        /**
         * Service Components
         */
        public final static String IT_SERVICE_COMPONENTS = "C__CATG__IT_SERVICE_COMPONENTS";

        /**
         * Service Logbook
         */
        public final static String ITS_LOGBOOK = "C__CATG__ITS_LOGBOOK";

        /**
         * Service Assignment
         */
        public final static String IT_SERVICE = "C__CATG__IT_SERVICE";

        /**
         * Object Vitality
         */
        public final static String OBJECT_VITALITY = "C__CATG__OBJECT_VITALITY";

        /**
         * Relationship
         */
        public final static String RELATION = "C__CATG__RELATION";

        /**
         * Service Relation
         */
        public final static String IT_SERVICE_RELATIONS = "C__CATG__IT_SERVICE_RELATIONS";

        /**
         * Database Assignment
         */
        public final static String DATABASE_ASSIGNMENT = "C__CATG__DATABASE_ASSIGNMENT";

        /**
         * Service Type
         */
        public final static String ITS_TYPE = "C__CATG__ITS_TYPE";

        /**
         * Passwords
         */
        public final static String PASSWD = "C__CATG__PASSWD";

        /**
         * Soa-stacks
         */
        public final static String SOA_STACKS = "C__CATG__SOA_STACKS";

        /**
         * Status-planning
         */
        public final static String PLANNING = "C__CATG__PLANNING";

        /**
         * Assigned Cards
         */
        public final static String ASSIGNED_CARDS = "C__CATG__ASSIGNED_CARDS";

        /**
         * Sim Card
         */
        public final static String SIM_CARD = "C__CATG__SIM_CARD";

        /**
         * Tsi Service
         */
        public final static String TSI_SERVICE = "C__CATG__TSI_SERVICE";

        /**
         * Audit
         */
        public final static String AUDIT = "C__CATG__AUDIT";

        /**
         * Port Overview
         */
        public final static String SUBCAT_NETWORK_PORT_OVERVIEW = "C__CMDB__SUBCAT__NETWORK_PORT_OVERVIEW";

        /**
         * Logical Location
         */
        public final static String LOGICAL_UNIT = "C__CATG__LOGICAL_UNIT";

        /**
         * Assigned Logical Units
         */
        public final static String ASSIGNED_LOGICAL_UNIT = "C__CATG__ASSIGNED_LOGICAL_UNIT";

        /**
         * Assigned Workstation
         */
        public final static String ASSIGNED_WORKSTATION = "C__CATG__ASSIGNED_WORKSTATION";

        /**
         * All Tickets
         */
        public final static String VIRTUAL_TICKETS = "C__CATG__VIRTUAL_TICKETS";

        /**
         * Assigned Workplaces
         */
        public final static String PERSON_ASSIGNED_WORKSTATION = "C__CATG__PERSON_ASSIGNED_WORKSTATION";

        /**
         * Contract Assignment
         */
        public final static String CONTRACT_ASSIGNMENT = "C__CATG__CONTRACT_ASSIGNMENT";

        /**
         * Assigned Chassis
         */
        public final static String CHASSIS_ASSIGNMENT = "C__CATG__CHASSIS_ASSIGNMENT";

        /**
         * Stacking
         */
        public final static String STACKING = "C__CATG__STACKING";

        /**
         * Racks
         */
        public final static String RACK_VIEW = "C__CATG__RACK_VIEW";

        /**
         * E-mail Addresses
         */
        public final static String MAIL_ADDRESSES = "C__CATG__MAIL_ADDRESSES";

        /**
         * Cucm Voip Telephone
         */
        public final static String VOIP_PHONE = "C__CATG__VOIP_PHONE";

        /**
         * Cucm Voip Line
         */
        public final static String VOIP_PHONE_LINE = "C__CATG__VOIP_PHONE_LINE";

        /**
         * Telephone/fax
         */
        public final static String TELEPHONE_FAX = "C__CATG__TELEPHONE_FAX";

        /**
         * Smart Card Certificate
         */
        public final static String SMARTCARD_CERTIFICATE = "C__CATG__SMARTCARD_CERTIFICATE";

        /**
         * Share Access
         */
        public final static String SHARE_ACCESS = "C__CATG__SHARE_ACCESS";

        /**
         * Supernet
         */
        public final static String VIRTUAL_SUPERNET = "C__CATG__VIRTUAL_SUPERNET";

        /**
         * Certificate
         */
        public final static String CERTIFICATE = "C__CATG__CERTIFICATE";

        /**
         * Sla
         */
        public final static String SLA = "C__CATG__SLA";

        /**
         * Ldap
         */
        public final static String LDAP_DN = "C__CATG__LDAP_DN";

        /**
         * Access Permissions
         */
        public final static String VIRTUAL_AUTH = "C__CATG__VIRTUAL_AUTH";

        /**
         * Host Definition
         */
        public final static String NAGIOS = "C__CATG__NAGIOS";

        /**
         * Nagios Group
         */
        public final static String NAGIOS_GROUP = "C__CATG__NAGIOS_GROUP";

        /**
         * Nagios (service)
         */
        public final static String NAGIOS_SERVICE_FOLDER = "C__CATG__NAGIOS_SERVICE_FOLDER";

        /**
         * Nagios (service Tpl)
         */
        public final static String NAGIOS_SERVICE_TPL_FOLDER = "C__CATG__NAGIOS_SERVICE_TPL_FOLDER";

        /**
         * Service Definition
         */
        public final static String NAGIOS_SERVICE_DEF = "C__CATG__NAGIOS_SERVICE_DEF";

        /**
         * Backwards Service Assignment
         */
        public final static String NAGIOS_REFS_SERVICES_BACKWARDS = "C__CATG__NAGIOS_REFS_SERVICES_BACKWARDS";

        /**
         * Service-template Definition
         */
        public final static String NAGIOS_SERVICE_TPL_DEF = "C__CATG__NAGIOS_SERVICE_TPL_DEF";

        /**
         * Assigned Objects
         */
        public final static String NAGIOS_SERVICE_REFS_TPL_BACKWARDS = "C__CATG__NAGIOS_SERVICE_REFS_TPL_BACKWARDS";

        /**
         * Nagios (host Tpl)
         */
        public final static String NAGIOS_HOST_TPL_FOLDER = "C__CATG__NAGIOS_HOST_TPL_FOLDER";

        /**
         * Host-template Definition
         */
        public final static String NAGIOS_HOST_TPL_DEF = "C__CATG__NAGIOS_HOST_TPL_DEF";

        /**
         * Nagios (host)
         */
        public final static String NAGIOS_HOST_FOLDER = "C__CATG__NAGIOS_HOST_FOLDER";

        /**
         * Assigned Objects
         */
        public final static String NAGIOS_HOST_TPL_ASSIGNED_OBJECTS = "C__CATG__NAGIOS_HOST_TPL_ASSIGNED_OBJECTS";

        /**
         * Service Assignment
         */
        public final static String NAGIOS_REFS_SERVICES = "C__CATG__NAGIOS_REFS_SERVICES";

        /**
         * Nagios (application)
         */
        public final static String NAGIOS_APPLICATION_FOLDER = "C__CATG__NAGIOS_APPLICATION_FOLDER";

        /**
         * Service Assignment
         */
        public final static String NAGIOS_APPLICATION_REFS_NAGIOS_SERVICE = "C__CATG__NAGIOS_APPLICATION_REFS_NAGIOS_SERVICE";

        /**
         * Service Dependencies
         */
        public final static String NAGIOS_SERVICE_DEP = "C__CATG__NAGIOS_SERVICE_DEP";

        /**
         * Viva
         */
        public final static String VIRTUAL_VIVA = "C__CATG__VIRTUAL_VIVA";

        /**
         * Address
         */
        public final static String ADDRESS = "C__CATG__ADDRESS";

        /**
         * Monitoring
         */
        public final static String MONITORING = "C__CATG__MONITORING";

        /**
         * Lc__cmdb__catg__virtual__rfc
         */
        public final static String VIRTUAL__RFC = "C__CATG__VIRTUAL__RFC";

        /**
         * Lc__catg__livestatus
         */
        public final static String LIVESTATUS = "C__CATG__LIVESTATUS";

        /**
         * Lc__catg__ndo
         */
        public final static String NDO = "C__CATG__NDO";

        /**
         * Vehicle
         */
        public final static String VEHICLE = "C__CATG__VEHICLE";

        /**
         * Aircraft
         */
        public final static String AIRCRAFT = "C__CATG__AIRCRAFT";

        /**
         * Network Connections
         */
        public final static String NET_CONNECTIONS_FOLDER = "C__CATG__NET_CONNECTIONS_FOLDER";

        /**
         * Listener
         */
        public final static String NET_LISTENER = "C__CATG__NET_LISTENER";

        /**
         * Connection
         */
        public final static String NET_CONNECTOR = "C__CATG__NET_CONNECTOR";

        /**
         * Administration Service
         */
        public final static String CLUSTER_ADM_SERVICE = "C__CATG__CLUSTER_ADM_SERVICE";

        /**
         * Lc__cmdb__catg__jdisc_custom_attributes
         */
        public final static String JDISC_CA = "C__CATG__JDISC_CA";

        /**
         * Check_mk (host)
         */
        public final static String CMK = "C__CATG__CMK";

        /**
         * Host Tags
         */
        public final static String CMK_TAG = "C__CATG__CMK_TAG";

        /**
         * Service Assignment
         */
        public final static String CMK_HOST_SERVICE = "C__CATG__CMK_HOST_SERVICE";

        /**
         * Service Assignment
         */
        public final static String CMK_SERVICE = "C__CATG__CMK_SERVICE";

        /**
         * Export Parameter
         */
        public final static String CMK_DEF = "C__CATG__CMK_DEF";

        /**
         * Cable
         */
        public final static String CABLE = "C__CATG__CABLE";

        /**
         * Lc__cmdb__catg__document
         */
        public final static String DOCUMENT = "C__CATG__DOCUMENT";

        /**
         * Custom Identifier
         */
        public final static String IDENTIFIER = "C__CATG__IDENTIFIER";

        /**
         * Services
         */
        public final static String SERVICE = "C__CATG__SERVICE";

        /**
         * Operating System
         */
        public final static String OPERATING_SYSTEM = "C__CATG__OPERATING_SYSTEM";

        /**
         * Lc__cmdb__catg__jdisc_discovery
         */
        public final static String JDISC_DISCOVERY = "C__CATG__JDISC_DISCOVERY";

        /**
         * Qinq Sp-vlan
         */
        public final static String QINQ_SP = "C__CATG__QINQ_SP";

        /**
         * Fiber/lead
         */
        public final static String FIBER_LEAD = "C__CATG__FIBER_LEAD";

        /**
         * Qinq Ce-vlan
         */
        public final static String QINQ_CE = "C__CATG__QINQ_CE";

        /**
         * Relocations
         */
        public final static String VIRTUAL_RELOCATE_CI = "C__CATG__VIRTUAL_RELOCATE_CI";
    }

    public interface S {

        /**
         * Rack
         */
        public final static String ENCLOSURE = "C__CATS__ENCLOSURE";

        /**
         * Room
         */
        public final static String ROOM = "C__CATS__ROOM";

        /**
         * Services
         */
        public final static String SERVICE = "C__CATS__SERVICE";

        /**
         * Switch
         */
        public final static String SWITCH_NET = "C__CATS__SWITCH_NET";

        /**
         * Wan
         */
        public final static String WAN = "C__CATS__WAN";

        /**
         * Emergency Plan
         */
        public final static String EMERGENCY_PLAN = "C__CATS__EMERGENCY_PLAN";

        /**
         * Air Conditioning
         */
        public final static String AC = "C__CATS__AC";

        /**
         * Wifi Device
         */
        public final static String ACCESS_POINT = "C__CATS__ACCESS_POINT";

        /**
         * Monitor
         */
        public final static String MONITOR = "C__CATS__MONITOR";

        /**
         * Desktop
         */
        public final static String CLIENT = "C__CATS__CLIENT";

        /**
         * Fc Switch
         */
        public final static String SWITCH_FC = "C__CATS__SWITCH_FC";

        /**
         * Routing
         */
        public final static String ROUTER = "C__CATS__ROUTER";

        /**
         * Printer
         */
        public final static String PRT = "C__CATS__PRT";

        /**
         * Files
         */
        public final static String FILE = "C__CATS__FILE";

        /**
         * Applications
         */
        public final static String APPLICATION = "C__CATS__APPLICATION";

        /**
         * Net
         */
        public final static String NET = "C__CATS__NET";

        /**
         * Mobile Radio
         */
        public final static String CELL_PHONE_CONTRACT = "C__CATS__CELL_PHONE_CONTRACT";

        /**
         * Licenses
         */
        public final static String LICENCE = "C__CATS__LICENCE";

        /**
         * Object Group
         */
        public final static String GROUP = "C__CATS__GROUP";

        /**
         * License Keys
         */
        public final static String SUBCAT_LICENCE_LIST = "C__CMDB__SUBCAT__LICENCE_LIST";

        /**
         * Overview
         */
        public final static String SUBCAT_LICENCE_OVERVIEW = "C__CMDB__SUBCAT__LICENCE_OVERVIEW";

        /**
         * Current File
         */
        public final static String SUBCAT_FILE_ACTUAL = "C__CMDB__SUBCAT__FILE_ACTUAL";

        /**
         * File Versions
         */
        public final static String SUBCAT_FILE_VERSIONS = "C__CMDB__SUBCAT__FILE_VERSIONS";

        /**
         * Assigned Objects
         */
        public final static String SUBCAT_FILE_OBJECTS = "C__CMDB__SUBCAT__FILE_OBJECTS";

        /**
         * Emergency Plan Properties
         */
        public final static String SUBCAT_EMERGENCY_PLAN = "C__CMDB__SUBCAT__EMERGENCY_PLAN";

        /**
         * Assigned Objects
         */
        public final static String SUBCAT_EMERGENCY_PLAN_LINKED_OBJECT_LIST = "C__CMDB__SUBCAT__EMERGENCY_PLAN_LINKED_OBJECT_LIST";

        /**
         * Net Type
         */
        public final static String SUBCAT_WS_NET_TYPE = "C__CMDB__SUBCAT__WS_NET_TYPE";

        /**
         * Assigned Objects
         */
        public final static String SUBCAT_WS_ASSIGNMENT = "C__CMDB__SUBCAT__WS_ASSIGNMENT";

        /**
         * Wiring System
         */
        public final static String WS = "C__CATS__WS";

        /**
         * Uninterruptible Power Supply
         */
        public final static String UPS = "C__CATS__UPS";

        /**
         * Emergency Power Supply
         */
        public final static String EPS = "C__CATS__EPS";

        /**
         * San Zoning
         */
        public final static String SAN_ZONING = "C__CATS__SAN_ZONING";

        /**
         * Organization
         */
        public final static String ORGANIZATION = "C__CATS__ORGANIZATION";

        /**
         * Master Data
         */
        public final static String ORGANIZATION_MASTER_DATA = "C__CATS__ORGANIZATION_MASTER_DATA";

        /**
         * Persons
         */
        public final static String ORGANIZATION_PERSONS = "C__CATS__ORGANIZATION_PERSONS";

        /**
         * Persons
         */
        public final static String PERSON = "C__CATS__PERSON";

        /**
         * Master Data
         */
        public final static String PERSON_MASTER = "C__CATS__PERSON_MASTER";

        /**
         * Login
         */
        public final static String PERSON_LOGIN = "C__CATS__PERSON_LOGIN";

        /**
         * Person Group Memberships
         */
        public final static String PERSON_ASSIGNED_GROUPS = "C__CATS__PERSON_ASSIGNED_GROUPS";

        /**
         * Person Groups
         */
        public final static String PERSON_GROUP = "C__CATS__PERSON_GROUP";

        /**
         * Master Data
         */
        public final static String PERSON_GROUP_MASTER = "C__CATS__PERSON_GROUP_MASTER";

        /**
         * Members
         */
        public final static String PERSON_GROUP_MEMBERS = "C__CATS__PERSON_GROUP_MEMBERS";

        /**
         * Assigned Objects
         */
        public final static String ORGANIZATION_CONTACT_ASSIGNMENT = "C__CATS__ORGANIZATION_CONTACT_ASSIGNMENT";

        /**
         * Assigned Objects
         */
        public final static String PERSON_CONTACT_ASSIGNMENT = "C__CATS__PERSON_CONTACT_ASSIGNMENT";

        /**
         * Assigned Objects
         */
        public final static String PERSON_GROUP_CONTACT_ASSIGNMENT = "C__CATS__PERSON_GROUP_CONTACT_ASSIGNMENT";

        /**
         * Assigned Clusters
         */
        public final static String CLUSTER_SERVICE = "C__CATS__CLUSTER_SERVICE";

        /**
         * Relation Details
         */
        public final static String RELATION_DETAILS = "C__CATS__RELATION_DETAILS";

        /**
         * Database Schema
         */
        public final static String DATABASE_SCHEMA = "C__CATS__DATABASE_SCHEMA";

        /**
         * Database Links
         */
        public final static String DATABASE_LINKS = "C__CATS__DATABASE_LINKS";

        /**
         * Dbms
         */
        public final static String DBMS = "C__CATS__DBMS";

        /**
         * Instance / Oracle Database
         */
        public final static String DATABASE_INSTANCE = "C__CATS__DATABASE_INSTANCE";

        /**
         * Pdu
         */
        public final static String PDU = "C__CATS__PDU";

        /**
         * Branch
         */
        public final static String PDU_BRANCH = "C__CATS__PDU_BRANCH";

        /**
         * Overview
         */
        public final static String PDU_OVERVIEW = "C__CATS__PDU_OVERVIEW";

        /**
         * Parallel Relations
         */
        public final static String PARALLEL_RELATION = "C__CATS__PARALLEL_RELATION";

        /**
         * Database Objects
         */
        public final static String DATABASE_OBJECTS = "C__CATS__DATABASE_OBJECTS";

        /**
         * Database Access
         */
        public final static String DATABASE_ACCESS = "C__CATS__DATABASE_ACCESS";

        /**
         * Database Gateway
         */
        public final static String DATABASE_GATEWAY = "C__CATS__DATABASE_GATEWAY";

        /**
         * Replication
         */
        public final static String REPLICATION = "C__CATS__REPLICATION";

        /**
         * Replication Partner
         */
        public final static String REPLICATION_PARTNER = "C__CATS__REPLICATION_PARTNER";

        /**
         * Installation
         */
        public final static String APPLICATION_ASSIGNED_OBJ = "C__CATS__APPLICATION_ASSIGNED_OBJ";

        /**
         * Cluster Installation
         */
        public final static String CLUSTER_SERVICE_ASSIGNED_OBJ = "C__CATS__CLUSTER_SERVICE_ASSIGNED_OBJ";

        /**
         * Middleware
         */
        public final static String MIDDLEWARE = "C__CATS__MIDDLEWARE";

        /**
         * Crypto Card
         */
        public final static String KRYPTO_CARD = "C__CATS__KRYPTO_CARD";

        /**
         * Ip List
         */
        public final static String NET_IP_ADDRESSES = "C__CATS__NET_IP_ADDRESSES";

        /**
         * Dhcp
         */
        public final static String NET_DHCP = "C__CATS__NET_DHCP";

        /**
         * Layer 2 Net
         */
        public final static String LAYER2_NET = "C__CATS__LAYER2_NET";

        /**
         * Assigned Ports
         */
        public final static String LAYER2_NET_ASSIGNED_PORTS = "C__CATS__LAYER2_NET_ASSIGNED_PORTS";

        /**
         * Contract
         */
        public final static String CONTRACT = "C__CATS__CONTRACT";

        /**
         * Contract Information
         */
        public final static String CONTRACT_INFORMATION = "C__CATS__CONTRACT_INFORMATION";

        /**
         * Assigned Objects
         */
        public final static String CONTRACT_ALLOCATION = "C__CATS__CONTRACT_ALLOCATION";

        /**
         * Chassis
         */
        public final static String CHASSIS = "C__CATS__CHASSIS";

        /**
         * Slots
         */
        public final static String CHASSIS_SLOT = "C__CATS__CHASSIS_SLOT";

        /**
         * Assigned Devices
         */
        public final static String CHASSIS_DEVICES = "C__CATS__CHASSIS_DEVICES";

        /**
         * Chassis View
         */
        public final static String CHASSIS_VIEW = "C__CATS__CHASSIS_VIEW";

        /**
         * Cabling
         */
        public final static String CHASSIS_CABLING = "C__CATS__CHASSIS_CABLING";

        /**
         * Variants
         */
        public final static String APPLICATION_VARIANT = "C__CATS__APPLICATION_VARIANT";

        /**
         * Authorization Config
         */
        public final static String BASIC_AUTH = "C__CATS__BASIC_AUTH";

        /**
         * Nagios
         */
        public final static String PERSON_NAGIOS = "C__CATS__PERSON_NAGIOS";

        /**
         * Nagios
         */
        public final static String PERSON_GROUP_NAGIOS = "C__CATS__PERSON_GROUP_NAGIOS";

        /**
         * Type
         */
        public final static String GROUP_TYPE = "C__CATS__GROUP_TYPE";

        /**
         * Assigned Logical Ports
         */
        public final static String LAYER2_NET_ASSIGNED_LOGICAL_PORTS = "C__CATS__LAYER2_NET_ASSIGNED_LOGICAL_PORTS";

        /**
         * Installation
         */
        public final static String APPLICATION_SERVICE_ASSIGNED_OBJ = "C__CATS__APPLICATION_SERVICE_ASSIGNED_OBJ";

        /**
         * Installation
         */
        public final static String APPLICATION_DBMS_ASSIGNED_OBJ = "C__CATS__APPLICATION_DBMS_ASSIGNED_OBJ";
    }
}
