/*
 * The MIT License
 *
 * Copyright 2016 Thomas Wittek <thomas.wittek@shd-online.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.idoit.api.cmdb;

/**
 *
 * @author Thomas Wittek <thomas.wittek@shd-online.de>
 */
public interface ObjectTypeConstants {

    /**
     * System Service
     */
    public final static String TYPE_SERVICE = "C__OBJTYPE__SERVICE";

    /**
     * Application
     */
    public final static String TYPE_APPLICATION = "C__OBJTYPE__APPLICATION";

    /**
     * Building
     */
    public final static String TYPE_BUILDING = "C__OBJTYPE__BUILDING";

    /**
     * Rack
     */
    public final static String TYPE_ENCLOSURE = "C__OBJTYPE__ENCLOSURE";

    /**
     * Server
     */
    public final static String TYPE_SERVER = "C__OBJTYPE__SERVER";

    /**
     * Switch
     */
    public final static String TYPE_SWITCH = "C__OBJTYPE__SWITCH";

    /**
     * Router
     */
    public final static String TYPE_ROUTER = "C__OBJTYPE__ROUTER";

    /**
     * Fc Switch
     */
    public final static String TYPE_FC_SWITCH = "C__OBJTYPE__FC_SWITCH";

    /**
     * Storage System
     */
    public final static String TYPE_SAN = "C__OBJTYPE__SAN";

    /**
     * Client
     */
    public final static String TYPE_CLIENT = "C__OBJTYPE__CLIENT";

    /**
     * Printer
     */
    public final static String TYPE_PRINTER = "C__OBJTYPE__PRINTER";

    /**
     * Air Condition System
     */
    public final static String TYPE_AIR_CONDITION_SYSTEM = "C__OBJTYPE__AIR_CONDITION_SYSTEM";

    /**
     * Wan
     */
    public final static String TYPE_WAN = "C__OBJTYPE__WAN";

    /**
     * Emergency Plan
     */
    public final static String TYPE_EMERGENCY_PLAN = "C__OBJTYPE__EMERGENCY_PLAN";

    /**
     * Kvm Switch
     */
    public final static String TYPE_KVM_SWITCH = "C__OBJTYPE__KVM_SWITCH";

    /**
     * Monitor
     */
    public final static String TYPE_MONITOR = "C__OBJTYPE__MONITOR";

    /**
     * Appliance
     */
    public final static String TYPE_APPLIANCE = "C__OBJTYPE__APPLIANCE";

    /**
     * Telephone System
     */
    public final static String TYPE_TELEPHONE_SYSTEM = "C__OBJTYPE__TELEPHONE_SYSTEM";

    /**
     * Printbox
     */
    public final static String TYPE_PRINTBOX = "C__OBJTYPE__PRINTBOX";

    /**
     * Room
     */
    public final static String TYPE_ROOM = "C__OBJTYPE__ROOM";

    /**
     * Wireless Access Point
     */
    public final static String TYPE_ACCESS_POINT = "C__OBJTYPE__ACCESS_POINT";

    /**
     * Contract
     */
    public final static String TYPE_MAINTENANCE = "C__OBJTYPE__MAINTENANCE";

    /**
     * File
     */
    public final static String TYPE_FILE = "C__OBJTYPE__FILE";

    /**
     * Generic Location
     */
    public final static String TYPE_LOCATION_GENERIC = "C__OBJTYPE__LOCATION_GENERIC";

    /**
     * Layer 3-net
     */
    public final static String TYPE_LAYER3_NET = "C__OBJTYPE__LAYER3_NET";

    /**
     * Cellular Phone
     */
    public final static String TYPE_CELL_PHONE_CONTRACT = "C__OBJTYPE__CELL_PHONE_CONTRACT";

    /**
     * Licenses
     */
    public final static String TYPE_LICENCE = "C__OBJTYPE__LICENCE";

    /**
     * Container
     */
    public final static String TYPE_CONTAINER = "C__OBJTYPE__CONTAINER";

    /**
     * Operating System
     */
    public final static String TYPE_OPERATING_SYSTEM = "C__OBJTYPE__OPERATING_SYSTEM";

    /**
     * Object Group
     */
    public final static String TYPE_GROUP = "C__OBJECT_TYPE__GROUP";

    /**
     * Generic Template
     */
    public final static String TYPE_GENERIC_TEMPLATE = "C__OBJTYPE__GENERIC_TEMPLATE";

    /**
     * Phone
     */
    public final static String TYPE_PHONE = "C__OBJTYPE__PHONE";

    /**
     * Host
     */
    public final static String TYPE_HOST = "C__OBJTYPE__HOST";

    /**
     * Cable
     */
    public final static String TYPE_CABLE = "C__OBJTYPE__CABLE";

    /**
     * Converter
     */
    public final static String TYPE_CONVERTER = "C__OBJTYPE__CONVERTER";

    /**
     * Wiring System
     */
    public final static String TYPE_WIRING_SYSTEM = "C__OBJTYPE__WIRING_SYSTEM";

    /**
     * Patch Panel
     */
    public final static String TYPE_PATCH_PANEL = "C__OBJTYPE__PATCH_PANEL";

    /**
     * Amplifier
     */
    public final static String TYPE_AMPLIFIER = "C__OBJTYPE__AMPLIFIER";

    /**
     * Service
     */
    public final static String TYPE_IT_SERVICE = "C__OBJTYPE__IT_SERVICE";

    /**
     * Electric Power Company
     */
    public final static String TYPE_ESC = "C__OBJTYPE__ESC";

    /**
     * Emergency Power Supply
     */
    public final static String TYPE_EPS = "C__OBJTYPE__EPS";

    /**
     * Distribution Box
     */
    public final static String TYPE_DISTRIBUTION_BOX = "C__OBJTYPE__DISTRIBUTION_BOX";

    /**
     * Power Distribution Unit
     */
    public final static String TYPE_PDU = "C__OBJTYPE__PDU";

    /**
     * Uninterruptible Power Supply
     */
    public final static String TYPE_UPS = "C__OBJTYPE__UPS";

    /**
     * San Zoning
     */
    public final static String TYPE_SAN_ZONING = "C__OBJTYPE__SAN_ZONING";

    /**
     * Organization
     */
    public final static String TYPE_ORGANIZATION = "C__OBJTYPE__ORGANIZATION";

    /**
     * Persons
     */
    public final static String TYPE_PERSON = "C__OBJTYPE__PERSON";

    /**
     * Person Groups
     */
    public final static String TYPE_PERSON_GROUP = "C__OBJTYPE__PERSON_GROUP";

    /**
     * Cluster
     */
    public final static String TYPE_CLUSTER = "C__OBJTYPE__CLUSTER";

    /**
     * Cluster Services
     */
    public final static String TYPE_CLUSTER_SERVICE = "C__OBJTYPE__CLUSTER_SERVICE";

    /**
     * Virtual Client
     */
    public final static String TYPE_VIRTUAL_CLIENT = "C__OBJTYPE__VIRTUAL_CLIENT";

    /**
     * Virtual Host
     */
    public final static String TYPE_VIRTUAL_HOST = "C__OBJTYPE__VIRTUAL_HOST";

    /**
     * Virtual Server
     */
    public final static String TYPE_VIRTUAL_SERVER = "C__OBJTYPE__VIRTUAL_SERVER";

    /**
     * Relation
     */
    public final static String TYPE_RELATION = "C__OBJTYPE__RELATION";

    /**
     * Dbms
     */
    public final static String TYPE_DBMS = "C__OBJTYPE__DBMS";

    /**
     * Database Schema
     */
    public final static String TYPE_DATABASE_SCHEMA = "C__OBJTYPE__DATABASE_SCHEMA";

    /**
     * Parallel Relations
     */
    public final static String TYPE_PARALLEL_RELATION = "C__OBJTYPE__PARALLEL_RELATION";

    /**
     * Replication Object
     */
    public final static String TYPE_REPLICATION = "C__OBJTYPE__REPLICATION";

    /**
     * Database Instance
     */
    public final static String TYPE_DATABASE_INSTANCE = "C__OBJTYPE__DATABASE_INSTANCE";

    /**
     * Middleware
     */
    public final static String TYPE_MIDDLEWARE = "C__OBJTYPE__MIDDLEWARE";

    /**
     * Soa Stack
     */
    public final static String TYPE_SOA_STACK = "C__OBJTYPE__SOA_STACK";

    /**
     * Crypto Card
     */
    public final static String TYPE_KRYPTO_CARD = "C__OBJTYPE__KRYPTO_CARD";

    /**
     * Sim Card
     */
    public final static String TYPE_SIM_CARD = "C__OBJTYPE__SIM_CARD";

    /**
     * Layer 2 Net
     */
    public final static String TYPE_LAYER2_NET = "C__OBJTYPE__LAYER2_NET";

    /**
     * Workplace
     */
    public final static String TYPE_WORKSTATION = "C__OBJTYPE__WORKSTATION";

    /**
     * Migration Objects
     */
    public final static String TYPE_MIGRATION_OBJECT = "C__OBJTYPE__MIGRATION_OBJECT";

    /**
     * Switch Chassis
     */
    public final static String TYPE_SWITCH_CHASSIS = "C__OBJTYPE__SWITCH_CHASSIS";

    /**
     * Blade Chassis
     */
    public final static String TYPE_BLADE_CHASSIS = "C__OBJTYPE__BLADE_CHASSIS";

    /**
     * Blade Server
     */
    public final static String TYPE_BLADE_SERVER = "C__OBJTYPE__BLADE_SERVER";

    /**
     * Voip Telephone
     */
    public final static String TYPE_VOIP_PHONE = "C__OBJTYPE__VOIP_PHONE";

    /**
     * Supernet
     */
    public final static String TYPE_SUPERNET = "C__OBJTYPE__SUPERNET";

    /**
     * Nagios Service
     */
    public final static String TYPE_NAGIOS_SERVICE = "C__OBJTYPE__NAGIOS_SERVICE";

    /**
     * Nagios Service-template
     */
    public final static String TYPE_NAGIOS_SERVICE_TPL = "C__OBJTYPE__NAGIOS_SERVICE_TPL";

    /**
     * Nagios Host-template
     */
    public final static String TYPE_NAGIOS_HOST_TPL = "C__OBJTYPE__NAGIOS_HOST_TPL";

    /**
     * Information Domain
     */
    public final static String TYPE_INFORMATION_DOMAIN = "C__OBJTYPE__INFORMATION_DOMAIN";

    /**
     * Vehicle
     */
    public final static String TYPE_VEHICLE = "C__OBJTYPE__VEHICLE";

    /**
     * Aircraft
     */
    public final static String TYPE_AIRCRAFT = "C__OBJTYPE__AIRCRAFT";

    /**
     * Vrrp/hsrp Cluster
     */
    public final static String TYPE_CLUSTER_VRRP_HSRP = "C__OBJTYPE__CLUSTER_VRRP_HSRP";

    /**
     * Country
     */
    public final static String TYPE_COUNTRY = "C__OBJTYPE__COUNTRY";

    /**
     * City
     */
    public final static String TYPE_CITY = "C__OBJTYPE__CITY";

    /**
     * Remote Management Controller
     */
    public final static String TYPE_RM_CONTROLLER = "C__OBJTYPE__RM_CONTROLLER";

    /**
     * VRRP
     */
    public final static String TYPE_VRRP = "C__OBJTYPE__VRRP";

    /**
     * Stacking
     */
    public final static String TYPE_STACKING = "C__OBJTYPE__STACKING";

    /**
     * Cable Tray
     */
    public final static String TYPE_C__CMDB__OBJTYPE__CABLE_TRAY = "C__CMDB__OBJTYPE__CABLE_TRAY";

    /**
     * Conduit
     */
    public final static String TYPE_C__CMDB__OBJTYPE__CONDUIT = "C__CMDB__OBJTYPE__CONDUIT";
}
