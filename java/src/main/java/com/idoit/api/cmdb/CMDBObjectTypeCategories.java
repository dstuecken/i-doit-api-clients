/*
 * The MIT License
 *
 * Copyright 2016 Thomas Wittek <thomas.wittek@shd-online.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.idoit.api.cmdb;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Thomas Wittek <thomas.wittek@shd-online.de>
 */
public class CMDBObjectTypeCategories {

    private Map<String, Category> categoryMap;

    public CMDBObjectTypeCategories(JSONObject jsonObject) {
        init(jsonObject);
    }

    private Map<String, Category> getCategoryMap() {
        if (categoryMap == null) {
            categoryMap = new HashMap<>();
        }
        return categoryMap;
    }

    public Collection<Category> categories() {
        return getCategoryMap().values();
    }

    public boolean hasCategory(String catId) {
        return getCategoryMap().containsKey(catId);
    }

    public Category getCategory(String catId) {
        return getCategoryMap().get(catId);
    }

    private void init(JSONObject jsonObject) {
        String[] categoryTypes = {"catg", "cats", "custom"};
        for (String categoryType : categoryTypes) {
            if (jsonObject.has(categoryType)) {
                JSONArray categoryObjects = jsonObject.optJSONArray(categoryType);
                for (int i = 0; i < categoryObjects.length(); i++) {
                    Category category = new Category(categoryObjects.optJSONObject(i));
                    getCategoryMap().put(category.getID(), category);
                }
            }
        }
    }

    public class Category {

        private String id;
        private String title;
        private String constant;
        private String parent;
        private boolean multiValue;
        private String sourceTable;

        public Category(JSONObject jsonObject) {
            init(jsonObject);
        }

        public String getID() {
            return id;
        }

        public String getTitle() {
            return title;
        }

        public String getConstant() {
            return constant;
        }

        public String getParent() {
            return parent;
        }

        public boolean isMultiValue() {
            return multiValue;
        }

        public String getSourceTable() {
            return sourceTable;
        }

        private void init(JSONObject jsonObject) {
            this.id = jsonObject.optString("id", "");
            this.title = jsonObject.optString("title", "");
            this.constant = jsonObject.optString("const", "");
            this.parent = jsonObject.optString("parent", "");
            this.multiValue = jsonObject.optString("multi_value", "0").equals("1");
            this.parent = jsonObject.optString("source_table", "");
        }
    }
}
