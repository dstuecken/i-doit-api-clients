/*
 * The MIT License
 *
 * Copyright 2016 Thomas Wittek <thomas.wittek@shd-online.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.idoit.api;

import com.idoit.api.cmdb.category.CategoryAttribute;
import com.idoit.api.cmdb.category.CMDBCategoryInfo;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Thomas Wittek <thomas.wittek@shd-online.de>
 */
public class CategoryInfoModule implements MethodConstants {

    private final IdoitClient client;

    public CategoryInfoModule(IdoitClient client) {
        this.client = client;
    }

    /**
     * Reading Structural data / attributes of a particular category
     * 
     * @see CMDBCategoryInfo
     * 
     * @param category the category ID or constant 
     * 
     * @return CMDBCategoryInfo
     * @throws IdoitClientException 
     */
    public CMDBCategoryInfo read(String category) throws IdoitClientException {
        try {
            JSONObject params = new JSONObject();
            params.put("category", category);
            JSONObject responseObject = client.sendRequest(CMDB_CATEGORY_INFO, params, "1");
            return createCMDBCategoryAttributes(category, responseObject);
        } catch (JSONException ex) {
            throw new IdoitClientException(ex.getLocalizedMessage(), ex.getCause());
        }
    }

    private CMDBCategoryInfo createCMDBCategoryAttributes(String constant, JSONObject jsonObject) throws IdoitClientException {
        try {
            CMDBCategoryInfo categoryInfo = new CMDBCategoryInfo();
            categoryInfo.setConstant(constant);

            // atributes
            JSONObject resultObject = jsonObject.optJSONObject("result");
            if (resultObject != null) {
                for (Iterator iterator = resultObject.keys(); iterator.hasNext();) {
                    String key = (String) iterator.next();
                    categoryInfo.addAttribute(createCMDBCategoryAttribute(resultObject.getJSONObject(key)));
                }
            }
            return categoryInfo;
        } catch (JSONException ex) {
            throw new IdoitClientException(ex.getLocalizedMessage(), jsonObject.toString());
        }
    }

    private CategoryAttribute createCMDBCategoryAttribute(JSONObject jsonObject) throws IdoitClientException {
        try {
            CategoryAttribute attribute = new CategoryAttribute();
            attribute.setTitle(jsonObject.optString("title", ""));

            // info
            JSONObject infoObject = jsonObject.getJSONObject("info");
            attribute.getInfo().setPrimaryField(infoObject.optBoolean("primary_field", false));
            attribute.getInfo().setType(infoObject.optString("type", ""));
            attribute.getInfo().setBackward(infoObject.optBoolean("backward", false));
            attribute.getInfo().setTitle(infoObject.optString("title", ""));
            attribute.getInfo().setDescription(infoObject.optString("description", ""));

            // data
            JSONObject dataObject = jsonObject.getJSONObject("data");
            attribute.getData().setType(dataObject.optString("type", ""));
            attribute.getData().setReadonly(dataObject.optBoolean("readonly", false));
            attribute.getData().setField(dataObject.optString("field", ""));

            JSONArray referencesArray = dataObject.optJSONArray("references");
            if (referencesArray != null) {
                for (int i = 0; i < referencesArray.length(); i++) {
                    attribute.getData().getReferences().add(referencesArray.getString(i));
                }
            }

            // ui
            JSONObject uiObject = jsonObject.getJSONObject("ui");
            attribute.getUi().setType(uiObject.optString("type", ""));
            attribute.getUi().setID(uiObject.optString("id", ""));
            attribute.getUi().setDefaultValue(uiObject.opt("default"));

            JSONObject paramsObject = uiObject.optJSONObject("params");
            if (paramsObject != null) {
                for (Iterator iterator = paramsObject.keys(); iterator.hasNext();) {
                    String key = (String) iterator.next();
                    attribute.getUi().getParams().put(key, paramsObject.get(key));
                }
            }

            // check
            JSONObject checkObject = jsonObject.optJSONObject("check");
            attribute.setMandatory(checkObject != null ? checkObject.optBoolean("mandatory", false) : false);

            // format
            JSONObject formatObject = jsonObject.optJSONObject("format");
            if (formatObject != null) {
                JSONArray callbackArray = formatObject.optJSONArray("callback");
                if (callbackArray != null) {
                    for (int i = 0; i < callbackArray.length(); i++) {
                        attribute.getCallback().add(callbackArray.get(i));
                    }
                }
            }
            return attribute;
        } catch (JSONException ex) {
            throw new IdoitClientException(ex.getLocalizedMessage(), jsonObject.toString());
        }
    }
}
