/*
 * The MIT License
 *
 * Copyright 2016 Thomas Wittek <thomas.wittek@shd-online.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.idoit.api;

import com.idoit.api.cmdb.CMDBObject;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Thomas Wittek <thomas.wittek@shd-online.de>
 */
public class ObjectModule implements MethodConstants {

    private final IdoitClient client;

    public ObjectModule(IdoitClient client) {
        this.client = client;
    }

    /**
     * Create a object.
     *
     * @param objectTypeConstant the object type ID or constant
     * @param title the object title
     *
     * @see CMDBObject
     * @return the created object
     * @throws IdoitClientException
     */
    public CMDBObject create(String objectTypeConstant, String title) throws IdoitClientException {
        try {
            JSONObject params = new JSONObject();
            params.put("type", objectTypeConstant);
            params.put("title", title);
            JSONObject responseObject = client.sendRequest(CMDB_OBJECT_CREATE, params, "1");
            JSONObject resultObject = responseObject.getJSONObject("result");
            return read(String.valueOf(resultObject.getInt("id")));
        } catch (JSONException ex) {
            throw new IdoitClientException(ex.getLocalizedMessage(), ex.getCause());
        }
    }

    /**
     * Read a object with the given ID.
     *
     * @param id the object ID
     *
     * @see CMDBObject
     * @return the object with the given ID
     * @throws IdoitClientException
     */
    public CMDBObject read(String id) throws IdoitClientException {
        try {
            JSONObject params = new JSONObject();
            params.put("id", id);
            JSONObject responseObject = client.sendRequest(CMDB_OBJECT_READ, params, "1");
            return createCMDBObject(responseObject);
        } catch (JSONException ex) {
            throw new IdoitClientException(ex.getLocalizedMessage(), ex.getCause());
        }
    }

    /**
     * Update a object with the given ID.
     *
     * @param id the object ID
     * @param newTitle new object title
     *
     * @see CMDBObject
     * @return update successful
     * @throws IdoitClientException
     */
    public boolean update(String id, String newTitle) throws IdoitClientException {
        try {
            JSONObject params = new JSONObject();
            params.put("id", id);
            params.put("title", newTitle);
            JSONObject responseObject = client.sendRequest(CMDB_OBJECT_UPDATE, params, "1");
            JSONObject resultObject = responseObject.getJSONObject("result");
            return resultObject.getBoolean("success");
        } catch (JSONException ex) {
            throw new IdoitClientException(ex.getLocalizedMessage(), ex.getCause());
        }
    }

    /**
     * Delete a object with the given ID.
     *
     * @param id the object ID
     *
     * @see CMDBObject
     * @return delete successful
     * @throws IdoitClientException
     */
    public boolean delete(String id) throws IdoitClientException {
        try {
            JSONObject params = new JSONObject();
            params.put("id", id);
            JSONObject responseObject = client.sendRequest(CMDB_OBJECT_DELETE, params, "1");
            JSONObject resultObject = responseObject.getJSONObject("result");
            return resultObject.getBoolean("success");
        } catch (JSONException ex) {
            throw new IdoitClientException(ex.getLocalizedMessage(), ex.getCause());
        }
    }

    private CMDBObject createCMDBObject(JSONObject jsonObject) throws IdoitClientException {
        try {
            JSONObject resultObject = jsonObject.getJSONObject("result");
            CMDBObject cmdbObject = new CMDBObject();
            cmdbObject.setID(resultObject.optString("id", ""));
            cmdbObject.setTitle(resultObject.optString("title", ""));
            cmdbObject.setSysID(resultObject.optString("sysid", ""));
            cmdbObject.setObjectType(resultObject.optString("objecttype", ""));
            cmdbObject.setObjectTypeTitle(resultObject.optString("type_title", ""));
            cmdbObject.setObjectTypeIconPath(resultObject.optString("type_icon", ""));
            cmdbObject.setStatus(resultObject.optString("status", ""));
            cmdbObject.setCMDBStatus(resultObject.optString("cmdb_status", ""));
            cmdbObject.setCMDBStatusTitle(resultObject.optString("cmdb_status_title", ""));
            cmdbObject.setCreated(resultObject.optString("created", ""));
            cmdbObject.setUpdated(resultObject.optString("updated", ""));
            cmdbObject.setImageUrl(resultObject.optString("image", ""));
            return cmdbObject;
        } catch (JSONException ex) {
            throw new IdoitClientException(ex.getLocalizedMessage(), jsonObject.toString());
        }
    }
}
