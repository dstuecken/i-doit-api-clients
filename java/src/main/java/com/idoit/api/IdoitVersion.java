/*
 * The MIT License
 *
 * Copyright 2016 Thomas Wittek <thomas.wittek@shd-online.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.idoit.api;

import org.json.JSONObject;

/**
 * @version 1.0
 * @author Thomas Wittek <thomas.wittek@shd-online.de>
 */
public class IdoitVersion {

    private String userId;
    private String name;
    private String mail;
    private String username;
    private String mandator;
    private String language;
    private String version;
    private String step;
    private String type;

    public IdoitVersion(JSONObject jsonObject) {
        init(jsonObject);
    }

    public String getUserID() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public String getMail() {
        return mail;
    }

    public String getUsername() {
        return username;
    }

    public String getMandator() {
        return mandator;
    }

    public String getLanguage() {
        return language;
    }

    public String getVersion() {
        return version;
    }

    public String getStep() {
        return step;
    }

    public String getType() {
        return type;
    }

    private void init(JSONObject jsonObject) {
        JSONObject resultObject = jsonObject.optJSONObject("result");
        if (resultObject != null) {
            JSONObject loginObject = resultObject.optJSONObject("login");
            if (loginObject != null) {
                this.userId = loginObject.optString("userid", "");
                this.name = loginObject.optString("name", "");
                this.mail = loginObject.optString("mail", "");
                this.username = loginObject.optString("username", "");
                this.mandator = loginObject.optString("mandator", "");
                this.language = loginObject.optString("language", "");
            }
            this.version = resultObject.optString("version", "");
            this.step = resultObject.optString("step", "");
            this.type = resultObject.optString("type", "");
        }
    }
}
