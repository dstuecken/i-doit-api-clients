/*
 * The MIT License
 *
 * Copyright 2016 Thomas Wittek <thomas.wittek@shd-online.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.idoit.api;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @version 0.6
 * @author Thomas Wittek <thomas.wittek@shd-online.de>
 */
public class IdoitClient implements MethodConstants {

    private final String IDOIT_JSON_RCP_PATH = "/src/jsonrpc.php";
    private final Client client;

    private String url;
    private String apiKey;
    private String sessionID;
    private String clientID;

    /**
     * The IdoitClient handles all the communication with the idoit API.
     *
     * @param url i-doit URL
     * @param apiKey i-doit API-KEY
     */
    public IdoitClient(String url, String apiKey) {
        this.url = url;
        this.apiKey = apiKey;
        this.client = ClientBuilder.newClient();
    }

    /**
     * The URL for the i-doit system
     *
     * @return i-doit URL
     */
    public String getUrl() {
        if (url == null) {
            url = "";
        }
        return url;
    }

    /**
     * The current session ID
     *
     * @return i-doit session ID
     */
    public String getSessionID() {
        if (sessionID == null) {
            sessionID = "";
        }
        return sessionID;
    }

    /**
     * The current client ID
     *
     * @return i-doit client ID
     */
    public String getClientID() {
        if (clientID == null) {
            clientID = "";
        }
        return clientID;
    }

    /**
     * The i-doit version information
     *
     * @see IdoitVersion
     *
     * @return i-doit version
     * @throws IdoitClientException
     */
    public IdoitVersion version() throws IdoitClientException {
        return new IdoitVersion(sendRequest(IDOIT_VERSION));
    }

    /**
     * @see IdoitConstants
     *
     * @return IdoitConstants
     * @throws IdoitClientException
     */
    public IdoitConstants constants() throws IdoitClientException {
        return new IdoitConstants(sendRequest(IDOIT_CONSTANTS));
    }

    /**
     * Create a i-doit session
     *
     * @param username the username
     * @param password the password
     * @throws IdoitClientException
     */
    public void login(String username, String password) throws IdoitClientException {
        try {
            WebTarget target = client.target(url + IDOIT_JSON_RCP_PATH);
            JSONObject request = createRequestObject(IDOIT_LOGIN, new JSONObject(), "1");
            MultivaluedMap<String, Object> authHeaders = new MultivaluedHashMap<>();
            authHeaders.add("X-RPC-Auth-Username", username);
            authHeaders.add("X-RPC-Auth-Password", password);
            Response response = target.request().headers(authHeaders).post(Entity.json(request.toString()));
            JSONObject responseObject = getResponseObject(response);
            JSONObject resultObject = responseObject.getJSONObject("result");
            sessionID = resultObject.getString("session-id");
            clientID = resultObject.getString("client-id");
        } catch (JSONException ex) {
            throw new IdoitClientException(ex.getLocalizedMessage(), ex.getCause());
        }
    }

    /**
     * Close the i-doit session
     *
     * @throws IdoitClientException
     */
    public void logout() throws IdoitClientException {
        try {
            JSONObject responseObject = sendRequest(IDOIT_LOGOUT);
            JSONObject resultObject = responseObject.getJSONObject("result");
            if (resultObject.getBoolean("result")) {
                sessionID = "";
                clientID = "";
            }
        } catch (JSONException ex) {
            throw new IdoitClientException(ex.getLocalizedMessage(), ex.getCause());
        }
    }

    /**
     * Access objectTypeGroups
     *
     * @return ObjectTypeGroupsModule
     */
    public ObjectTypeGroupModule objectTypeGroups() {
        return new ObjectTypeGroupModule(this);
    }

    /**
     * Access objectTypeCategories
     *
     * @return ObjectTypeCategoriesModule
     */
    public ObjectTypeCategoriesModule objectTypeCategories() {
        return new ObjectTypeCategoriesModule(this);
    }

    /**
     * Access objectTypes
     *
     * @return ObjectTypeModule
     */
    public ObjectTypeModule objectTypes() {
        return new ObjectTypeModule(this);
    }

    /**
     * Access object query
     *
     * @return ObjectTypeModule
     */
    public ObjectsModule objects() {
        return new ObjectsModule(this);
    }

    /**
     * Access objects
     *
     * @return ObjectTypeModule
     */
    public ObjectModule object() {
        return new ObjectModule(this);
    }

    /**
     * Access category info
     *
     * @return CategoryInfoModule
     */
    public CategoryInfoModule categoryInfo() {
        return new CategoryInfoModule(this);
    }

    /**
     * Access category
     *
     * @return CategoryModule
     */
    public CategoryModule category() {
        return new CategoryModule(this);
    }

    JSONObject sendRequest(String method) throws IdoitClientException {
        return sendRequest(method, new JSONObject(), "1");
    }

    JSONObject sendRequest(String method, String id) throws IdoitClientException {
        return sendRequest(method, new JSONObject(), id);
    }

    JSONObject sendRequest(String method, JSONObject params, String id) throws IdoitClientException {
        WebTarget target = client.target(url + IDOIT_JSON_RCP_PATH);
        JSONObject request = createRequestObject(method, params, id);

        // for debuging only
        // System.out.println("SEND REQUEST: " + request.toString());
        Response response = getSessionID().isEmpty()
                ? target.request().post(Entity.json(request.toString()))
                : target.request().header("X-RPC-Auth-Session", getSessionID()).post(Entity.json(request.toString()));
        return getResponseObject(response);
    }

    private JSONObject createRequestObject(String method, JSONObject params, String id) throws IdoitClientException {
        try {
            JSONObject requestObject = new JSONObject();
            requestObject.put("method", method);
            params.put("apikey", apiKey);
            requestObject.put("params", params);
            requestObject.put("id", id);
            requestObject.put("jsonrpc", "2.0");
            return requestObject;
        } catch (JSONException ex) {
            throw new IdoitClientException(ex.getLocalizedMessage(), ex.getCause());
        }
    }

    private JSONObject getResponseObject(Response response) throws IdoitClientException {
        try {
            if (response.getStatus() == 200) {
                String responseString = response.readEntity(String.class);

                // for debuging only
                // System.out.println("GET RESPONSE: " + responseString);
                JSONObject responseObject = new JSONObject(responseString);
                if (responseObject.has("error")) {
                    throw new IdoitClientException(responseObject);
                }
                return responseObject;
            } else {
                throw new IdoitClientException(response.getStatusInfo().getReasonPhrase());
            }
        } catch (JSONException ex) {
            throw new IdoitClientException(ex.getLocalizedMessage(), ex.getCause());
        }
    }
}
