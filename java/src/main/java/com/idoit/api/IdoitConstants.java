/*
 * The MIT License
 *
 * Copyright 2016 Thomas Wittek <thomas.wittek@shd-online.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.idoit.api;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONObject;

/**
 * Query of internal i-doit constants. These constants are required, for example
 * to objects of a particular type (C__OBJTYPE__XYZ) or categories of a certain
 * type (C__CAT [G | S] __ XYZ) to create or query. This allows, for example,
 * the consultation of the constant of a user-defined object type.
 *
 * @version 1.0
 * @author Thomas Wittek <thomas.wittek@shd-online.de>
 */
public class IdoitConstants {

    private Map<String, String> objectTypesMap;
    private CategoryConstants categories;

    public IdoitConstants(JSONObject jsonObject) {
        init(jsonObject);
    }

    /**
     * Get all object type constants.
     *
     * @return a map with all object type constants
     */
    public Map<String, String> objectTypes() {
        if (objectTypesMap == null) {
            objectTypesMap = new HashMap<>();
        }
        return objectTypesMap;
    }

    /**
     * Get access to all g and s category constants.
     *
     * @return CategoryConstants object contain maps with g and s category
     * constants
     */
    public CategoryConstants categories() {
        if (categories == null) {
            categories = new CategoryConstants();
        }
        return categories;
    }

    private void init(JSONObject jsonObject) {
        JSONObject resultObject = jsonObject.optJSONObject("result");
        if (resultObject != null) {
            JSONObject objectTypesObject = resultObject.optJSONObject("objectTypes");
            if (objectTypesObject != null) {
                for (Iterator iterator = objectTypesObject.keys(); iterator.hasNext();) {
                    String key = (String) iterator.next();
                    objectTypes().put(key, objectTypesObject.optString(key, ""));
                }
            }

            JSONObject categoriesObject = resultObject.optJSONObject("categories");
            if (categoriesObject != null) {
                JSONObject gObject = categoriesObject.optJSONObject("g");
                if (gObject != null) {
                    for (Iterator iterator = gObject.keys(); iterator.hasNext();) {
                        String key = (String) iterator.next();
                        categories().g().put(key, gObject.optString(key, ""));
                    }
                }
                JSONObject sObject = categoriesObject.optJSONObject("s");
                if (sObject != null) {
                    for (Iterator iterator = sObject.keys(); iterator.hasNext();) {
                        String key = (String) iterator.next();
                        categories().s().put(key, sObject.optString(key, ""));
                    }
                }
            }
        }
    }

    public class CategoryConstants {

        private Map<String, String> gMap;
        private Map<String, String> sMap;

        /**
         * Get all g-category constants.
         *
         * @return a map with all g-category constants
         */
        public Map<String, String> g() {
            if (gMap == null) {
                gMap = new HashMap<>();
            }
            return gMap;
        }

        /**
         * Get all s-category constants.
         *
         * @return a map with all s-category constants
         */
        public Map<String, String> s() {
            if (sMap == null) {
                sMap = new HashMap<>();
            }
            return sMap;
        }
    }
}
