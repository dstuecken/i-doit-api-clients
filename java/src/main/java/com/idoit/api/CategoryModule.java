/*
 * The MIT License
 *
 * Copyright 2016 Thomas Wittek <thomas.wittek@shd-online.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.idoit.api;

import com.idoit.api.cmdb.category.CMDBCategory;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Thomas Wittek <thomas.wittek@shd-online.de>
 */
public class CategoryModule implements MethodConstants {

    private final IdoitClient client;

    public CategoryModule(IdoitClient client) {
        this.client = client;
    }

    /**
     * Create data category of a particular object.
     *
     * @param objID the object ID
     * @param category the category ID or constant
     * @param newCategory the category object to create
     *
     * @see CMDBCategory
     * @return the created category
     * @throws IdoitClientException
     */
    public CMDBCategory create(String objID, String category, CMDBCategory newCategory) throws IdoitClientException {
        try {
            JSONObject params = new JSONObject();
            params.put("objID", objID);
            params.put("category", category);
            params.put("data", createDataObject(newCategory));
            JSONObject responseObject = client.sendRequest(CMDB_CATEGORY_CREATE, params, "1");
            JSONObject resultObject = responseObject.getJSONObject("result");
            CMDBCategory createdCategory = null;
            if (resultObject.optBoolean("success", false)) {
                createdCategory = read(objID, category, resultObject.optString("id", ""));
            }
            return createdCategory;
        } catch (JSONException ex) {
            throw new IdoitClientException(ex.getLocalizedMessage(), ex.getCause());
        }
    }

    /**
     * Read data category of a particular object.
     *
     * @param objID the object ID
     * @param category the category ID or constant
     *
     * @see CMDBCategory
     * @return a collection of category entries (single-value category is a
     * single entry collection)
     * @throws IdoitClientException
     */
    public Collection<CMDBCategory> read(String objID, String category) throws IdoitClientException {
        try {
            JSONObject params = new JSONObject();
            params.put("objID", objID);
            params.put("category", category);
            JSONObject responseObject = client.sendRequest(CMDB_CATEGORY_READ, params, "1");
            return createCMDBCategories(responseObject);
        } catch (JSONException ex) {
            throw new IdoitClientException(ex.getLocalizedMessage(), ex.getCause());
        }
    }

    /**
     * Read an entry of data category of a particular object.
     *
     * @param objID the object ID
     * @param category the category ID or constant
     * @param entryID the entry ID
     *
     * @see CMDBCategory
     * @return the category with the given entry ID (for multi-value categories)
     * @throws IdoitClientException
     */
    public CMDBCategory read(String objID, String category, String entryID) throws IdoitClientException {
        Collection<CMDBCategory> categories = read(objID, category);
        CMDBCategory resultCategory = null;
        for (CMDBCategory currentCategory : categories) {
            if (entryID.equals(currentCategory.getID())) {
                resultCategory = currentCategory;
                break;
            }
        }
        return resultCategory;
    }

    /**
     * Update data category of a particular object.
     *
     * @param objID the object ID
     * @param category the category ID or constant
     * @param newCategory the category object only contains the changes
     *
     * @see CMDBCategory
     * @return the updated category
     * @throws IdoitClientException
     */
    public CMDBCategory update(String objID, String category, CMDBCategory newCategory) throws IdoitClientException {
        try {
            JSONObject params = new JSONObject();
            params.put("objID", objID);
            params.put("category", category);
            params.put("data", createDataObject(newCategory));
            JSONObject responseObject = client.sendRequest(CMDB_CATEGORY_UPDATE, params, "1");
            JSONObject resultObject = responseObject.getJSONObject("result");
            CMDBCategory updatedCategory = null;
            if (resultObject.optBoolean("success", false)) {
                updatedCategory = read(objID, category, newCategory.getID());
            }
            return updatedCategory;
        } catch (JSONException ex) {
            throw new IdoitClientException(ex.getLocalizedMessage(), ex.getCause());
        }
    }

    /**
     * Delete an entry of data category of a particular object.
     *
     * @param objID the object ID
     * @param category the category ID or constant
     * @param entryID the entry ID
     *
     * @see CMDBCategory
     * @return delete successful
     * @throws IdoitClientException
     */
    public boolean delete(String objID, String category, String entryID) throws IdoitClientException {
        try {
            JSONObject params = new JSONObject();
            params.put("objID", objID);
            params.put("category", category);
            params.put("id", entryID);
            JSONObject responseObject = client.sendRequest(CMDB_CATEGORY_DELETE, params, "1");
            JSONObject resultObject = responseObject.getJSONObject("result");
            return resultObject.getBoolean("success");
        } catch (JSONException ex) {
            throw new IdoitClientException(ex.getLocalizedMessage(), ex.getCause());
        }
    }

    private Collection<CMDBCategory> createCMDBCategories(JSONObject jsonObject) throws IdoitClientException {
        try {
            Collection<CMDBCategory> categories = new ArrayList<>();

            JSONArray resultObjects = jsonObject.getJSONArray("result");
            for (int i = 0; i < resultObjects.length(); i++) {
                JSONObject resultObject = resultObjects.getJSONObject(i);
                categories.add(createCMDBCategory(resultObject));
            }
            return categories;
        } catch (JSONException ex) {
            throw new IdoitClientException(ex.getLocalizedMessage(), jsonObject.toString());
        }
    }

    private CMDBCategory createCMDBCategory(JSONObject jsonObject) throws IdoitClientException {
        try {
            CMDBCategory category = new CMDBCategory();

            for (Iterator keyIterator = jsonObject.keys(); keyIterator.hasNext();) {
                String key = keyIterator.next().toString();
                CMDBCategory.Value categoryValue = category.createValue(key);
                if (!jsonObject.isNull(key)) {
                    Object value = jsonObject.get(key);
                    if (value instanceof String) {
                        categoryValue.setTitle((String) value);
                    } else if (value instanceof JSONObject) {
                        JSONObject valueObject = (JSONObject) value;
                        for (Iterator objectKeyIterator = valueObject.keys(); objectKeyIterator.hasNext();) {
                            String objectKey = objectKeyIterator.next().toString();
                            categoryValue.putProperty(objectKey, valueObject.optString(objectKey, ""));
                        }
                    }
                }
            }
            return category;
        } catch (JSONException ex) {
            throw new IdoitClientException(ex.getLocalizedMessage(), jsonObject.toString());
        }
    }

    private JSONObject createDataObject(CMDBCategory newCategory) throws IdoitClientException {
        try {
            JSONObject dataObject = new JSONObject();
            for (CMDBCategory.Value value : newCategory.values()) {
                if (value.isSimpleValue()) {
                    dataObject.put(value.getKey(), value.getTitle());
                } else {
                    dataObject.put(value.getKey(), value.getID());
                }
            }
            return dataObject;
        } catch (JSONException ex) {
            throw new IdoitClientException(ex.getLocalizedMessage(), ex.getCause());
        }
    }
}
