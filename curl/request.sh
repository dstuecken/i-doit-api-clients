#!/bin/bash
#
# i-doit PHP API Client
# CURL package
#
# Author: Dennis Stücken
# License: MIT
#
# Usage:   ./request.sh {json-file}
#
# Example: ./request.sh snippets/cmdb/object.json
#
# The result is plain json.
#

API_ENDPOINT="http://demo.i-doit.com/src/jsonrpc.php"
BASE64_AUTH="YWRtaW46YWRtaW4=" # Base64 encoded string "admin:admin"

curl -i -X POST -H "Authorization:Basic ${BASE64_AUTH}" -H "Content-Type:application/json; charset=UTF-8" --data "@./${1}" '${API_ENDPOINT}'
